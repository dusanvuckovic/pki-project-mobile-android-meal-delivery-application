package pki.mobile.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.util.Linkify;
import android.util.Patterns;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;

import java.time.format.DateTimeFormatter;

import pki.mobile.model.DB;
import pki.mobile.model.Order;
import pki.mobile.pkimobile.R;

public class OrderActivity extends AppCompatActivity {

    private static final String KEY_ORDER = "ORDER";
    private Order order;

    private TextView username;
    private TextView address;
    private TextView deliverBy;
    private TextView phone;
    private TextView price;
    private SupportMapFragment map;

    public static Intent newInstance(Context context, int orderID) {
        Intent intent = new Intent(context, OrderActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_ORDER, orderID);
        intent.putExtras(bundle);
        return intent;
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            order = DB.getOrderFromID(b.getInt(KEY_ORDER));
        } else if (savedInstanceState != null) {
            order = DB.getOrderFromID(savedInstanceState.getInt(KEY_ORDER));
        }

        setContentView(R.layout.activity_order);
        getSupportActionBar().setTitle(String.format("Narudžbina #%d", order.getID()));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        username = findViewById(R.id.order_name);
        address = findViewById(R.id.order_address);
        deliverBy = findViewById(R.id.order_deliver_by);
        phone = findViewById(R.id.order_phone);
        price = findViewById(R.id.order_price);
        map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.order_map);

        username.setText(order.getUserWhoOrdered().getFullName());
        address.setText(order.getDeliveredTo());
        deliverBy.setText(String.format("Do: %s", order.getAcceptedOn().plusMinutes(45).format(DateTimeFormatter.ofPattern("HH:mm:ss"))));
        phone.setText(order.getUserWhoOrdered().getPhone());
        price.setText(String.format("Cena: %d RSD", order.calculatePrice()));
        Linkify.addLinks(phone, Patterns.PHONE, "tel:");

        map.getMapAsync(googleMap -> {
            googleMap.setMyLocationEnabled(true);
            googleMap.addMarker(new MarkerOptions().draggable(false).position(order.getDeliveryLatLng()));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(order.getDeliveryLatLng(), 15));
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_ORDER, order.getID());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
