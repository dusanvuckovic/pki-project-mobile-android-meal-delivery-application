package pki.mobile.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.maps.model.LatLng;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

import pki.mobile.fragments.FindOrdersFragment;
import pki.mobile.fragments.MapViewFragment;
import pki.mobile.fragments.PersonalDataFragment;
import pki.mobile.fragments.ViewDeliveriesFragment;
import pki.mobile.helper.DirectionsHelper;
import pki.mobile.model.DB;
import pki.mobile.model.Order;
import pki.mobile.model.Restaurant;
import pki.mobile.pkimobile.R;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;


public class DeliveryActivity extends NavigableActivity {

    public static final int
            FIND_ORDERS_FRAGMENT = 0,
            VIEW_DELIVERIES_FRAGMENT = 1,
            MAP_VIEW_FRAGMENT = 2,
            PERSONAL_DATA_FRAGMENT = 3;
    private static final int REQUEST_LOCATION_PERMISSIONS = 0;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter sectionsPagerAdapter;
    private FusedLocationProviderClient fpc;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private LatLng myLocation = null;

    public static Intent newInstance(Context context) {
        return new Intent(context, DeliveryActivity.class);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSIONS) {
            if (grantResults.length == 2 && grantResults[0] == PERMISSION_GRANTED && grantResults[1] == PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    fpc.requestLocationUpdates(locationRequest, new LocationCallback(), null);
                }
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();
        locationRequest = DirectionsHelper.createLocationRequest();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, permissions, 0);
            return;
        }
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location l = locationResult.getLastLocation();
                myLocation = new LatLng(l.getLatitude(), l.getLongitude());

                Map<Restaurant, Double> restaurantMap = new HashMap<>();
                for (Order o : DB.orders) {
                    if (o.getStatus() != Order.Status.DELIVERED && o.getStatus() != Order.Status.FAILED) {
                        Restaurant r = o.getRestaurantDelivering();
                        if (restaurantMap.containsKey(r)) {
                            o.setDelivererToRestaurantKm(restaurantMap.get(r));
                        } else {
                            double d = DirectionsHelper.getDirectionsHelper().
                                    getDistanceInMeters(DirectionsHelper.makeLatLng(myLocation), o.getRestaurantDelivering().getLatLng());
                            restaurantMap.put(r, d);
                            o.setDelivererToRestaurantKm(d);
                        }
                    }
                }

                update();
            }
        };
        fpc.requestLocationUpdates(locationRequest, locationCallback, null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fpc.removeLocationUpdates(locationCallback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_buttons, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logoff:
                DB.logOff();
                Intent intent = UnregisteredActivity.newInstance(getApplicationContext());
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);
        fpc = LocationServices.getFusedLocationProviderClient(getApplicationContext());
        locationRequest = DirectionsHelper.createLocationRequest();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, permissions, 0);
            return;
        }
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location l = locationResult.getLastLocation();
                myLocation = new LatLng(l.getLatitude(), l.getLongitude());

                Map<Restaurant, Double> restaurantMap = new HashMap<>();
                for (Order o : DB.orders) {
                    if (o.getStatus() != Order.Status.DELIVERED && o.getStatus() != Order.Status.FAILED) {
                        Restaurant r = o.getRestaurantDelivering();
                        if (restaurantMap.containsKey(r)) {
                            o.setDelivererToRestaurantKm(restaurantMap.get(r));
                        } else {
                            double d = DirectionsHelper.getDirectionsHelper().
                                    getDistanceInMeters(DirectionsHelper.makeLatLng(myLocation), o.getRestaurantDelivering().getLatLng());
                            restaurantMap.put(r, d);
                            o.setDelivererToRestaurantKm(d);
                        }
                    }
                }

                update();
            }
        };

        Toolbar toolbar = findViewById(R.id.toolbar_delivery);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        viewPager = findViewById(R.id.container_delivery);
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(5);

        TabLayout tabLayout = findViewById(R.id.tabs_delivery);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        /*FloatingActionButton fab = findViewById(R.id.fab_delivery);
        fab.setOnClickListener(v -> {
            DB.logOff();
            DB.loginCustomer();
            Intent intent = CustomerActivity.newInstance(getApplicationContext());
            DeliveryActivity.this.startActivity(intent);
            DeliveryActivity.this.finish();
        });*/
    }

    public void update() {

        sectionsPagerAdapter.findOrdersFragment.update();
        sectionsPagerAdapter.viewDeliveriesFragment.update();
        updateMap();
    }

    public void updateMap() {

        Deque<com.google.android.gms.maps.model.LatLng> listLocs = new ArrayDeque<>();
        if (sectionsPagerAdapter.viewDeliveriesFragment.hasOrderPositions()) {
            listLocs = new ArrayDeque<>(sectionsPagerAdapter.viewDeliveriesFragment.getOrderPositions());
            if (!listLocs.isEmpty() && myLocation != null) {
                listLocs.addFirst(DirectionsHelper.makeLatLng(this.myLocation));
                sectionsPagerAdapter.mapViewFragment.update(listLocs);
            }
        } else {
            for (Order o : DB.getAcceptedOrders()) {
                listLocs.add(o.getRestaurantDelivering().getLatLng());
                listLocs.add(o.getDeliveryLatLng());
            }
        }
        if (!listLocs.isEmpty() && myLocation != null) {
            listLocs.addFirst(DirectionsHelper.makeLatLng(this.myLocation));
            if (sectionsPagerAdapter != null)
                sectionsPagerAdapter.mapViewFragment.update(listLocs);
        }


    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final FindOrdersFragment findOrdersFragment;
        private final ViewDeliveriesFragment viewDeliveriesFragment;
        private final MapViewFragment mapViewFragment;
        private final PersonalDataFragment personalDataFragment;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            findOrdersFragment = FindOrdersFragment.newInstance();
            viewDeliveriesFragment = ViewDeliveriesFragment.newInstance();
            mapViewFragment = MapViewFragment.newInstance();
            personalDataFragment = PersonalDataFragment.newInstance();
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return findOrdersFragment;
                case 1:
                    return viewDeliveriesFragment;
                case 2:
                    return mapViewFragment;
                case 3:
                    return personalDataFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 4;
        }
    }


}
