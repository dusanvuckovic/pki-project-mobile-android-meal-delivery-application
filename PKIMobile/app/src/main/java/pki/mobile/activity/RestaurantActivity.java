package pki.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import pki.mobile.fragments.restaurant_part_fragments.RestaurantFragment1Meals;
import pki.mobile.fragments.restaurant_part_fragments.RestaurantFragment2About;
import pki.mobile.fragments.restaurant_part_fragments.RestaurantFragment3Contact;
import pki.mobile.model.DB;
import pki.mobile.model.Restaurant;
import pki.mobile.pkimobile.R;

public class RestaurantActivity extends AppCompatActivity {


    private static final String KEY_RESTAURANT_NAME = "RESTAURANT_NAME";
    private static final String KEY_CAN_ORDER = "CAN_ORDER";

    private Restaurant restaurant = null;
    private String restaurantTitle = null;
    private boolean canOrder = false;

    public static Intent newInstance(Context context, String restaurantName, boolean canOrder) {
        Intent intent = new Intent(context, RestaurantActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(KEY_RESTAURANT_NAME, restaurantName);
        bundle.putBoolean(KEY_CAN_ORDER, canOrder);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            restaurant = DB.getRestaurantFromTitle(b.getString(KEY_RESTAURANT_NAME));
            DB.selectedRestaurant = restaurant;
            restaurantTitle = restaurant.getTitle();
            canOrder = b.getBoolean(KEY_CAN_ORDER);
        }

        setContentView(R.layout.activity_restaurant);
        if (restaurant != null)
            getSupportActionBar().setTitle(restaurant.getTitle());
        else
            getSupportActionBar().setTitle("Testing...");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(item -> {
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction transaction = fm.beginTransaction();
            switch (item.getItemId()) {
                case R.id.navigation_meals:
                    transaction.replace(R.id.restaurant_frame, RestaurantFragment1Meals.newInstance(restaurantTitle, canOrder));
                    transaction.commit();
                    return true;
                case R.id.navigation_about:
                    transaction.replace(R.id.restaurant_frame, RestaurantFragment2About.newInstance(restaurantTitle));
                    transaction.commit();
                    return true;
                case R.id.navigation_contact:
                    transaction.replace(R.id.restaurant_frame, RestaurantFragment3Contact.newInstance(restaurantTitle));
                    transaction.commit();
                    return true;
            }
            return false;
        });

        if (savedInstanceState == null)
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.restaurant_frame, RestaurantFragment1Meals.newInstance(restaurantTitle, canOrder))
                    .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
