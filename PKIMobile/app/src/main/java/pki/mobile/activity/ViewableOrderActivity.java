package pki.mobile.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.time.LocalDateTime;

import pki.mobile.model.DB;
import pki.mobile.model.Order;
import pki.mobile.pkimobile.R;

public class ViewableOrderActivity extends AppCompatActivity {

    private static final String KEY_ORDER_ID = "ORDER_ID";

    private Order o;
    private LatLng addressRestaurant;
    private LatLng addressEndUser;

    public static Intent newInstance(Context c, int orderID) {
        Intent intent = new Intent(c, ViewableOrderActivity.class);
        Bundle b = new Bundle();
        b.putInt(KEY_ORDER_ID, orderID);
        intent.putExtras(b);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewable_order);

        Bundle b = getIntent().getExtras();
        int orderID = b.getInt(KEY_ORDER_ID);
        o = DB.getOrderFromID(orderID);

        addressRestaurant = new LatLng(o.getRestaurantDelivering().getLatitude(), o.getRestaurantDelivering().getLongitude());
        addressEndUser = new LatLng(o.getDeliveryLatLng().latitude, o.getDeliveryLatLng().longitude);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(String.format("Narudžbina #%d", o.getID()));

        TextView userName = findViewById(R.id.viewable_order_user_first_and_last_name);
        TextView userAddress = findViewById(R.id.viewable_order_user_address);

        TextView restaurantName = findViewById(R.id.viewable_order_restaurant_name);
        TextView restaurantAddress = findViewById(R.id.viewable_order_restaurant_address);

        SupportMapFragment map = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.viewable_order_map);

        map.getMapAsync(googleMap -> {
            googleMap.addMarker(new MarkerOptions().title(o.getRestaurantDelivering().getTitle()).draggable(false).position(addressRestaurant));
            googleMap.addMarker(new MarkerOptions().title(o.getUserWhoOrdered().getFullName()).draggable(false).position(addressEndUser));
            LatLngBounds bounds = new LatLngBounds.Builder().include(addressRestaurant).include(addressEndUser).build();
            googleMap.setOnMapLoadedCallback(() -> googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100)));
        });
        Button accept = findViewById(R.id.viewable_order_button_accept);

        userName.setText(o.getUserWhoOrdered().getFullName());
        userAddress.setText(o.getDeliveredTo());

        restaurantName.setText(o.getRestaurantDelivering().getTitle());
        restaurantAddress.setText(o.getRestaurantDelivering().getAddress());

        accept.setOnClickListener(v -> {
            o.setStatus(Order.Status.ACCEPTED);
            o.setAcceptedOn(LocalDateTime.now());

            DB.loggedOn.getAcceptedOrders().add(o);
            setResult(Activity.RESULT_OK);
            finish();
        });

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
