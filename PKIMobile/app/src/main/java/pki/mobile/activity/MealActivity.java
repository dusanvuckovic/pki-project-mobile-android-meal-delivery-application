package pki.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.time.LocalDateTime;

import pki.mobile.adapters.CommentAdapter;
import pki.mobile.fragments.OrderDialogFragment;
import pki.mobile.model.Comment;
import pki.mobile.model.DB;
import pki.mobile.model.Meal;
import pki.mobile.model.Restaurant;
import pki.mobile.pkimobile.R;

public class MealActivity extends AppCompatActivity implements OrderDialogFragment.NoticeDialogListener {

    private static final String KEY_RESTAURANT_NAME = "RESTAURANT_NAME";
    private static final String KEY_MEAL_NAME = "MEAL_NAME";
    private static final String KEY_CAN_ORDER = "CAN_ORDER";

    private Restaurant restaurant;
    private Meal meal;
    private boolean canOrder;

    private ImageView mealImage;
    private TextView mealPrice;
    private TextView mealIngredients;
    private EditText mealWriteComment;
    private ImageButton mealSubmitComment;
    private ImageView mealShoppingCart;

    private ListView listView;

    public static Intent newInstance(Context context, String restaurantName, String mealName, boolean canOrder) {
        Intent intent = new Intent(context, MealActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(KEY_RESTAURANT_NAME, restaurantName);
        bundle.putString(KEY_MEAL_NAME, mealName);
        bundle.putBoolean(KEY_CAN_ORDER, canOrder);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            restaurant = DB.getRestaurantFromTitle(b.getString(KEY_RESTAURANT_NAME));
            meal = restaurant.getMealByName(b.getString(KEY_MEAL_NAME));
            canOrder = b.getBoolean(KEY_CAN_ORDER);
        }

        setContentView(R.layout.activity_meal);
        if (restaurant != null) {
            getSupportActionBar().setTitle(String.format("%s (%s)", meal.getName(), restaurant.getTitle()));
        } else
            getSupportActionBar().setTitle("Testing...");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mealImage = findViewById(R.id.meal_image);
        mealPrice = findViewById(R.id.meal_price);
        mealIngredients = findViewById(R.id.meal_ingredients);
        mealWriteComment = findViewById(R.id.meal_write_comment);
        mealSubmitComment = findViewById(R.id.meal_submit_comment);
        mealShoppingCart = findViewById(R.id.meal_shopping_cart);

        listView = findViewById(R.id.meal_listview);
        listView.setAdapter(new CommentAdapter(this, meal.getComments()));

        if (!meal.getImages().isEmpty())
            mealImage.setImageResource(meal.getImages().get(0));
        mealPrice.setText(String.format("%d RSD", meal.getPrice()));
        mealIngredients.setText(meal.getIngredientsInList());
        mealSubmitComment.setOnClickListener(v -> {
            String text = mealWriteComment.getText().toString().trim();
            if (text.isEmpty())
                return;
            Comment c = new Comment(text, DB.loggedOn.getUsername(), LocalDateTime.now());
            meal.getComments().add(c);
            ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
            mealWriteComment.setEnabled(false);
            mealWriteComment.setFocusable(false);
            mealWriteComment.setText("");
        });
        if (canOrder) {
            mealShoppingCart.setOnClickListener(v -> {
                OrderDialogFragment d = OrderDialogFragment.newInstance(restaurant.getTitle(), meal.getName());
                d.show(getSupportFragmentManager(), null);
            });
        } else {
            mealShoppingCart.setEnabled(false);
            mealShoppingCart.setFocusable(false);
            mealShoppingCart.setVisibility(View.GONE);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        Toast.makeText(this, "Jelo dodato u korpu!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
    }
}
