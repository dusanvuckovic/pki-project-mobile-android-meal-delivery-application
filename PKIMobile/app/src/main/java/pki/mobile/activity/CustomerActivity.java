package pki.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import pki.mobile.fragments.AllOrdersFragment;
import pki.mobile.fragments.CurrentOrderFragment;
import pki.mobile.fragments.PersonalDataFragment;
import pki.mobile.fragments.SearchFragment;
import pki.mobile.model.DB;
import pki.mobile.pkimobile.R;

public class CustomerActivity extends NavigableActivity {

    public static final int
            SEARCH_FRAGMENT = 0,
            CURRENT_ORDER_FRAGMENT = 1,
            ALL_ORDERS_FRAGMENT = 2,
            PERSONAL_DATA_FRAGMENT = 3;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter sectionsPagerAdapter;

    public static Intent newInstance(Context context) {
        return new Intent(context, CustomerActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        Toolbar toolbar = findViewById(R.id.toolbar_customer);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        viewPager = findViewById(R.id.container_customer);
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(5);

        TabLayout tabLayout = findViewById(R.id.tabs_customer);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        /*FloatingActionButton fab = findViewById(R.id.fab_customer);
        fab.setOnClickListener(view -> {
            DB.logOff();
            DB.loginDeliverer();
            Intent intent = DeliveryActivity.newInstance(getApplicationContext());
            CustomerActivity.this.startActivity(intent);
            CustomerActivity.this.finish();
        });*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_bar_buttons, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logoff:
                DB.logOff();
                Intent intent = UnregisteredActivity.newInstance(getApplicationContext());
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        update();
    }

    public void update() {
        sectionsPagerAdapter.update();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final SearchFragment searchFragment;
        private final CurrentOrderFragment currentOrderFragment;
        private final AllOrdersFragment allOrdersFragment;
        private final PersonalDataFragment personalDataFragment;


        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            searchFragment = SearchFragment.newInstance(true);
            currentOrderFragment = CurrentOrderFragment.newInstance();
            allOrdersFragment = AllOrdersFragment.newInstance();
            personalDataFragment = PersonalDataFragment.newInstance();
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return searchFragment;
                case 1:
                    return currentOrderFragment;
                case 2:
                    return allOrdersFragment;
                case 3:
                    return personalDataFragment;
            }
            return null;
        }

        public void update() {
            currentOrderFragment.update();
            allOrdersFragment.update();
        }

        @Override
        public int getCount() {
            return 4;
        }
    }
}
