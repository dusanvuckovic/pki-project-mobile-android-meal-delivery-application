package pki.mobile.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

public abstract class NavigableActivity extends AppCompatActivity {

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    protected ViewPager viewPager;

    public NavigableActivity() {
        super();
    }

    public ViewPager getViewPager() {
        return viewPager;
    }

    public void setTab(int tab) {
        viewPager.setCurrentItem(tab);
    }
}
