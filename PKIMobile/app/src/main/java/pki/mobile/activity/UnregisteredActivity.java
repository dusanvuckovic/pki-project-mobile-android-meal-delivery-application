package pki.mobile.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import pki.mobile.fragments.LoginFragment;
import pki.mobile.fragments.RegisterFragment;
import pki.mobile.fragments.SearchFragment;
import pki.mobile.fragments.register_part_fragments.RegisterFragment1UserPass;
import pki.mobile.fragments.register_part_fragments.RegisterFragment2Name;
import pki.mobile.fragments.register_part_fragments.RegisterFragment3Map;
import pki.mobile.model.DB;
import pki.mobile.model.User;
import pki.mobile.pkimobile.R;

public class UnregisteredActivity extends NavigableActivity {

    private static final String KEY_USER = "USER";
    private static final String TAG = "UnregisteredActivity";
    public static int LOGIN_FRAGMENT = 0, REGISTER_FRAGMENT = 1, SEARCH_FRAGMENT = 2;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter sectionsPagerAdapter;
    private RegisterFragment1UserPass registerFragment1UserPass;
    private RegisterFragment2Name registerFragment2Name;
    private RegisterFragment3Map registerFragment3Map;
    private User user;

    public static Intent newInstance(Context context) {
        return new Intent(context, UnregisteredActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unregistered);

        if (savedInstanceState != null) {
            user = (User) savedInstanceState.getSerializable(KEY_USER);
            registerFragment1UserPass = RegisterFragment1UserPass.newInstance(user.getUsername(), user.getPassword(), user.isDeliverer());
            registerFragment2Name = RegisterFragment2Name.newInstance(user.getFirstName(), user.getLastName(), user.getPhone());
            registerFragment3Map = RegisterFragment3Map.newInstance(user.getLatitude(), user.getLongitude(), user.getAddress());
        } else {
            user = new User();
            registerFragment1UserPass = RegisterFragment1UserPass.newInstance();
            registerFragment2Name = RegisterFragment2Name.newInstance();
            registerFragment3Map = RegisterFragment3Map.newInstance();
        }

        Toolbar toolbar = findViewById(R.id.toolbar_unregistered);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        viewPager = findViewById(R.id.container_unregistered);
        viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(5);

        TabLayout tabLayout = findViewById(R.id.tabs_unregistered);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(KEY_USER, user);
    }

    public void updateUserPass(String username, String password, int selectedRadioID) {
        user.setUsername(username);
        user.setPassword(password);
        user.setDeliverer(selectedRadioID == 1);
    }

    public void updateName(String firstName, String lastName, String phone) {
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setPhone(phone);
    }

    public void updateMap(String address, double latitude, double longitude) {
        user.setAddress(address);
        user.setLatitude(latitude);
        user.setLongitude(longitude);
    }

    public RegisterFragment1UserPass getRegisterFragment1UserPass() {
        return registerFragment1UserPass;
    }

    public RegisterFragment2Name getRegisterFragment2Name() {
        return registerFragment2Name;
    }

    public RegisterFragment3Map getRegisterFragment3Map() {
        return registerFragment3Map;
    }

    public User getUser() {
        return user;
    }

    public boolean isServicesOK() {
        Log.d(TAG, "Checking Google Services");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);

        if (available == ConnectionResult.SUCCESS) {
            Log.d(TAG, "Everything works");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            Log.d(TAG, "Problem exists, can be fixed");
            GoogleApiAvailability.getInstance().getErrorDialog(this, available, 9001).show();
        } else {
            Toast.makeText(this, "You can't make map requests!", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private final LoginFragment loginFragment;
        private final RegisterFragment registerFragment;
        private final SearchFragment searchFragment;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            loginFragment = LoginFragment.newInstance();
            registerFragment = RegisterFragment.newInstance();
            searchFragment = SearchFragment.newInstance(false);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return loginFragment;
                case 1:
                    return registerFragment;
                case 2:
                    return searchFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
