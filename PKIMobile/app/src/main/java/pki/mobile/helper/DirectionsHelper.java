package pki.mobile.helper;

import android.graphics.Color;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.DirectionsStep;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.DistanceMatrixRow;
import com.google.maps.model.Unit;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class DirectionsHelper {

    private static final Integer[] colors = {
            Color.BLUE, Color.RED, Color.GREEN, Color.MAGENTA, Color.CYAN, Color.GRAY
    };
    private static DirectionsHelper directionsHelper = null;
    private static int currentColor = 0;
    private static boolean endRoute = false;
    private final String KEY = "AIzaSyDTMdN6xyT4AVkOjoBU8VstevWzJnG3aNw";
    private final GeoApiContext geoApiContext;
    private DirectionsHelper() {
        geoApiContext = new GeoApiContext.Builder().apiKey(KEY).build();
    }

    public static DirectionsHelper getDirectionsHelper() {
        if (directionsHelper == null)
            directionsHelper = new DirectionsHelper();
        return directionsHelper;
    }

    private static int getNextColor() {
        return colors[currentColor];
    }

    private static void clearColors() {
        currentColor = 0;
    }

    private static void setNextColor() {
        if (endRoute) {
            currentColor = (currentColor + 1) % colors.length;
        }
        endRoute = !endRoute;
    }

    public static LocationRequest createLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    private static com.google.maps.model.LatLng makeModelLatLng(LatLng ll) {
        return new com.google.maps.model.LatLng(ll.latitude, ll.longitude);
    }

    public static LatLng makeLatLng(com.google.maps.model.LatLng ll) {
        return new LatLng(ll.lat, ll.lng);
    }

    public double getDistanceInMeters(LatLng startLocation, LatLng endLocation) {
        DirectionsApiRequest request = DirectionsApi.newRequest(geoApiContext);
        request.units(Unit.METRIC);
        request.optimizeWaypoints(false);
        request.origin(makeModelLatLng(startLocation));
        request.destination(makeModelLatLng(endLocation));

        DirectionsResult dr = null;
        try {
            dr = request.await();
        } catch (ApiException | IOException | InterruptedException e) {
            e.printStackTrace();
        }

        if (dr != null) {
            //requests without waypoints always return a single route with a single leg
            return dr.routes[0].legs[0].distance.inMeters;
        }
        return 0;
    }

    private DistanceMatrix getDistanceMatrix(LatLng startLocation, List<LatLng> locations) {
        DistanceMatrixApiRequest request = DistanceMatrixApi.newRequest(geoApiContext);
        request.units(Unit.METRIC);
        request.origins(makeModelLatLng(startLocation));
        request.destinations(makeModelDequeLatLng(new ArrayDeque<>(locations)).toArray(new com.google.maps.model.LatLng[locations.size()]));

        DistanceMatrix matrix = null;
        try {
            matrix = request.await();
        } catch (InterruptedException | ApiException | IOException e) {
            e.printStackTrace();
        }
        return matrix;
    }

    private DirectionsResult getDirections(LatLng startLocation, Deque<LatLng> locations) {
        DirectionsApiRequest request = DirectionsApi.newRequest(geoApiContext);
        request.optimizeWaypoints(false);
        request.units(Unit.METRIC);
        request.origin(makeModelLatLng(startLocation));
        request.destination(makeModelLatLng(locations.pollLast()));
        request.waypoints(this.makeModelDequeLatLng(locations).toArray(new com.google.maps.model.LatLng[locations.size()]));

        DirectionsResult dr = null;
        try {
            dr = request.await();
        } catch (ApiException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return dr;

    }

    public List<Route> getRoutes(List<LatLng> externalLocations) {
        Deque<LatLng> locations = new ArrayDeque<>(externalLocations);
        LatLng startLocation = locations.removeFirst();
        DirectionsResult dr = getDirections(startLocation, locations);
        List<Route> startEnds = new ArrayList<>();
        DirectionsRoute route = dr.routes[0];
        for (DirectionsLeg leg : route.legs) {
            for (DirectionsStep step : leg.steps) {
                startEnds.add(new Route(step.startLocation, step.endLocation));
            }
            setNextColor();
        }
        clearColors();
        return startEnds;
    }

    public List<Integer> getDistances(LatLng startLocation, List<LatLng> locations) {
        List<Integer> distances = new ArrayList<>();
        DistanceMatrix matrix = getDistanceMatrix(startLocation, locations);
        if (matrix != null) {
            DistanceMatrixRow row = matrix.rows[0];
            if (row != null) {
                for (DistanceMatrixElement elem : row.elements) {
                    distances.add((int) elem.distance.inMeters / 1000);
                }
            }
        }
        return distances;
    }

    private Deque<LatLng> makeDequeLatLng(Deque<com.google.maps.model.LatLng> deque) {
        Deque<LatLng> res = new ArrayDeque<>();
        for (com.google.maps.model.LatLng l : deque)
            res.add(makeLatLng(l));
        return res;
    }

    private Deque<com.google.maps.model.LatLng> makeModelDequeLatLng(Deque<LatLng> deque) {
        Deque<com.google.maps.model.LatLng> res = new ArrayDeque<>();
        for (LatLng l : deque)
            res.add(makeModelLatLng(l));
        return res;
    }

    public class Route {

        public final LatLng start;
        public final LatLng end;
        public final int color;
        Route(LatLng start, LatLng end) {
            this.start = start;
            this.end = end;
            this.color = getNextColor();
        }
        Route(com.google.maps.model.LatLng start, com.google.maps.model.LatLng end) {
            this.start = makeLatLng(start);
            this.end = makeLatLng(end);
            this.color = getNextColor();
        }
    }
}
