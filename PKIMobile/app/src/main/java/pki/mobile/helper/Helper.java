package pki.mobile.helper;

import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;


public class Helper {

    private Helper() {
    }

    public static void makeSnackbar(FragmentActivity activity, String text) {
        Snackbar.make(activity.findViewById(android.R.id.content), text, Snackbar.LENGTH_SHORT).show();
    }
}
