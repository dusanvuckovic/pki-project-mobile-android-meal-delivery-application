package pki.mobile.helper;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapGeocoder {

    private static final LatLng BELGRADE = new LatLng(44.787197, 20.457273);
    private final SupportMapFragment map;
    private final Geocoder geocoder;
    private final EditText addressText;
    private final View currentLocation;
    private final GoogleMap.OnMarkerDragListener markerDragListener = new GoogleMap.OnMarkerDragListener() {
        @Override
        public void onMarkerDragStart(Marker marker) {
            LatLng latLng = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);
            String address = getAddress(latLng);
            if (address != null)
                addressText.setText(address);

            map.getMapAsync(googleMap -> googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng)));
        }

        @Override
        public void onMarkerDrag(Marker marker) {
        }

        @Override
        public void onMarkerDragEnd(Marker marker) {

            LatLng latLng = new LatLng(marker.getPosition().latitude, marker.getPosition().longitude);
            String address = getAddress(latLng);
            if (address != null)
                addressText.setText(address);

            map.getMapAsync(googleMap -> googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng)));
        }
    };
    private Marker marker;

    public MapGeocoder(Context context, SupportMapFragment map,
                       LatLng startLatLng, String startAddress, boolean shouldBeDraggable,
                       EditText addressText, View currentLocation) {

        this.map = map;
        this.addressText = addressText;
        this.currentLocation = currentLocation;
        this.geocoder = new Geocoder(context, Locale.getDefault());

        map.getMapAsync(googleMap -> {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLatLng, 15));
            marker = googleMap.addMarker(new MarkerOptions()
                    .position(startLatLng)
                    .title("Beograd"));
            marker.setDraggable(shouldBeDraggable);
            if (shouldBeDraggable)
                googleMap.setOnMarkerDragListener(markerDragListener);
            addressText.setText(startAddress);
        });
        this.currentLocation.setOnClickListener(this::setAddress);
    }

    public MapGeocoder(Context context, SupportMapFragment map,
                       LatLng startLatLng, boolean shouldBeDraggable,
                       EditText addressText, View currentLocation) {

        this.map = map;
        this.addressText = addressText;
        this.currentLocation = currentLocation;
        this.geocoder = new Geocoder(context, Locale.getDefault());

        map.getMapAsync(googleMap -> {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLatLng, 15));
            marker = googleMap.addMarker(new MarkerOptions()
                    .position(startLatLng)
                    .title("Beograd"));
            marker.setDraggable(shouldBeDraggable);
            if (shouldBeDraggable)
                googleMap.setOnMarkerDragListener(markerDragListener);
            String address = getAddress(startLatLng);
            if (address != null)
                addressText.setText(address);
        });
        this.currentLocation.setOnClickListener(this::setAddress);
    }

    public MapGeocoder(Context context, SupportMapFragment map, boolean shouldBeDraggable,
                       EditText addressText, Button currentLocation) {
        this(context, map, BELGRADE, shouldBeDraggable,
                addressText, currentLocation);
    }

    public void moveMap(LatLng newLatLng) {
        marker.setPosition(newLatLng);
        map.getMapAsync(googleMap -> googleMap.moveCamera(CameraUpdateFactory.newLatLng(newLatLng)));
    }

    public void moveMap(LatLng newLatLng, EditText et) {
        if (marker == null || map == null)
            return;
        marker.setPosition(newLatLng);
        map.getMapAsync(googleMap -> {
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(newLatLng));
            et.setText(getAddress(newLatLng));
        });
    }

    private String getAddress(LatLng latlng) {
        return getAddress(latlng.latitude, latlng.longitude);
    }

    private String getAddress(double latitude, double longitude) {
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (IOException e) {
            e.printStackTrace();
        }
        String address = null;
        if (addresses != null && !addresses.isEmpty()) {
            Address a = addresses.get(0);
            address = a.getAddressLine(0);
        }
        return address;
    }

    private void setAddress(View v) {
        String locationName = addressText.getText().toString();
        Address a = null;
        try {
            List<Address> addresses = geocoder.getFromLocationName(locationName, 1);
            if (!addresses.isEmpty())
                a = addresses.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (a == null)
            return;
        LatLng latLng = new LatLng(a.getLatitude(), a.getLongitude());
        marker.setPosition(latLng);
        map.getMapAsync(googleMap -> googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng)));
    }


    public double getLatitude() {
        return marker.getPosition().latitude;
    }

    public double getLongitude() {
        return marker.getPosition().longitude;
    }

    public LatLng getLatLng() {
        return marker.getPosition();
    }
}
