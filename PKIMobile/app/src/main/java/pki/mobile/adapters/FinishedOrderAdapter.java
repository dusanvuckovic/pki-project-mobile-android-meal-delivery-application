package pki.mobile.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pki.mobile.model.Order;
import pki.mobile.pkimobile.R;

public class FinishedOrderAdapter extends ArrayAdapter<Order> {

    public FinishedOrderAdapter(@NonNull Context context, @NonNull List<Order> orders) {
        super(context, R.layout.graphic_finished_order, orders);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Order order = this.getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.graphic_finished_order, parent, false);
        }

        ImageView restaurantImage = convertView.findViewById(R.id.graphic_deliverable_image);
        TextView restaurantName = convertView.findViewById(R.id.graphic_deliverable_name);
        TextView price = convertView.findViewById(R.id.graphic_deliverable_price);
        TextView status = convertView.findViewById(R.id.graphic_deliverable_restaurant_status);
        TextView time = convertView.findViewById(R.id.graphic_finished_order_time);
        TextView userAddress = convertView.findViewById(R.id.graphic_deliverable_user_address);

        if (!order.getRestaurantDelivering().getImages().isEmpty())
            restaurantImage.setImageResource(order.getRestaurantDelivering().getImages().get(0));

        restaurantName.setText(order.getRestaurantDelivering().getTitle());
        price.setText(String.format("%d RSD", order.calculatePrice()));
        status.setText(order.getDescriptionFromStatus());
        time.setText(order.getTimeFromStatus());
        userAddress.setText(order.getDeliveredTo());

        return convertView;
    }

}
