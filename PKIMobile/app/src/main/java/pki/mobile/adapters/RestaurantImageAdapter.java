package pki.mobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.List;

import pki.mobile.pkimobile.R;

public class RestaurantImageAdapter extends ArrayAdapter<Integer> {

    public RestaurantImageAdapter(Context context, List<Integer> images) {
        super(context, R.layout.graphic_image, images);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Integer imageCode = this.getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.graphic_image, parent, false);
        }

        ImageView image = convertView.findViewById(R.id.graphic_image_imageview);
        image.setImageResource(imageCode);

        return convertView;
    }


}
