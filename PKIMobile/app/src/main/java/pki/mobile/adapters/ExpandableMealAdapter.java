package pki.mobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import pki.mobile.model.Meal;
import pki.mobile.model.Type;
import pki.mobile.pkimobile.R;

public class ExpandableMealAdapter extends BaseExpandableListAdapter {

    private final Context context;
    private final List<List<Meal>> meals;
    private final Map<Type, Integer> groupMap;
    private int currentID;

    public ExpandableMealAdapter(List<Meal> restaurantMeals, Context context) {
        this.context = context;
        meals = new ArrayList<>();
        groupMap = new HashMap<>();
        for (Meal m : restaurantMeals) {
            if (groupMap.containsKey(m.getType()))
                meals.get(groupMap.get(m.getType())).add(m);
            else {
                groupMap.put(m.getType(), currentID++);
                meals.add(new ArrayList<>(Arrays.asList(m)));
            }
        }
    }

    @Override
    public int getGroupCount() {
        return meals.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return meals.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return meals.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return meals.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View view, ViewGroup parent) {
        Map<Integer, Type> mapInverted =
                groupMap.entrySet()
                        .stream()
                        .collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey)
                        );
        Type t = mapInverted.get(groupPosition);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.meals_group_items, null);
        }
        TextView childItem = view.findViewById(R.id.heading);
        childItem.setText(t.getFullName());

        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View view, ViewGroup parent) {

        Meal meal = meals.get(groupPosition).get(childPosition);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.meals_child_items, null);
        }
        TextView childItem = view.findViewById(R.id.childItem);
        childItem.setText(meal.getName().trim());

        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
