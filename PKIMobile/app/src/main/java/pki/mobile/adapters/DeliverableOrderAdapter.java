package pki.mobile.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pki.mobile.model.Order;
import pki.mobile.pkimobile.R;


public class DeliverableOrderAdapter extends ArrayAdapter<Order> {


    public DeliverableOrderAdapter(@NonNull Context context, @NonNull List<Order> orders) {
        super(context, R.layout.graphic_deliverable_order, orders);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Order o = this.getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.graphic_deliverable_order, parent, false);
        }

        TextView username = convertView.findViewById(R.id.graphic_deliverable_username);
        TextView price = convertView.findViewById(R.id.graphic_deliverable_price);
        TextView restaurantName = convertView.findViewById(R.id.graphic_deliverable_name);
        TextView deliverableTime = convertView.findViewById(R.id.graphic_deliverable_time);
        TextView userAddress = convertView.findViewById(R.id.graphic_deliverable_user_address);
        TextView restaurantAddress = convertView.findViewById(R.id.graphic_deliverable_restaurant_status);
        TextView km = convertView.findViewById(R.id.graphic_deliverable_km);

        ImageView checkmark = convertView.findViewById(R.id.graphic_deliverable_checkmark);
        ImageView orderImage = convertView.findViewById(R.id.graphic_deliverable_image);

        username.setText(String.format("%s (%s)", o.getUserWhoOrdered().getFullName(), o.getRestaurantToUserDistanceKM()));
        price.setText(String.format("%d RSD", o.calculatePrice()));
        restaurantName.setText(String.format("%s (%s)", o.getRestaurantDelivering().getTitle(), o.getDelivererToRestaurantDistanceKM()));
        deliverableTime.setText(o.getTimeFromStatus());
        restaurantAddress.setText(o.getRestaurantDelivering().getAddress() + "TBD");
        userAddress.setText(o.getDeliveredTo());
        if (o.getRestaurantToUserDistance() != null && o.getDelivererToRestaurantDistance() != null)
            km.setText(String.format("%.2f km", o.getRestaurantToUserDistance() + o.getDelivererToRestaurantDistance()));
        else
            km.setText("računamo...");

        if (!o.getRestaurantDelivering().getImages().isEmpty())
            orderImage.setImageResource(o.getRestaurantDelivering().getImages().get(0));

        return convertView;
    }
}
