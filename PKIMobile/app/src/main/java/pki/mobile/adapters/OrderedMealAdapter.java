package pki.mobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import pki.mobile.fragments.CurrentOrderFragment;
import pki.mobile.model.OrderedMeal;
import pki.mobile.pkimobile.R;

public class OrderedMealAdapter extends ArrayAdapter<OrderedMeal> {

    private final List<OrderedMeal> orderedMeals;
    private final CurrentOrderFragment cof;

    public OrderedMealAdapter(Context context, List<OrderedMeal> orderedMeals, CurrentOrderFragment cof) {
        super(context, R.layout.graphic_ordered_meal, orderedMeals);
        this.orderedMeals = orderedMeals;
        this.cof = cof;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        OrderedMeal om = this.getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.graphic_ordered_meal, parent, false);
        }

        ImageView icon = convertView.findViewById(R.id.graphic_ordered_meal_icon);
        ImageView mealImage = convertView.findViewById(R.id.graphic_ordered_meal_image);
        TextView mealName = convertView.findViewById(R.id.graphic_ordered_meal_meal_name);
        TextView price = convertView.findViewById(R.id.graphic_ordered_meal_price);
        TextView restaurant = convertView.findViewById(R.id.graphic_ordered_meal_restaurant);

        icon.setOnClickListener(view -> {
            orderedMeals.remove(om);
            this.notifyDataSetChanged();
            cof.setAmount();
            cof.checkDisable();
        });

        if (!om.getMeal().getImages().isEmpty())
            mealImage.setImageResource(om.getMeal().getImages().get(0));

        mealName.setText(String.format("%s (%d)", om.getMeal().getName(), om.getHowMany()));
        price.setText(String.format("%d RSD", om.getOrderedMealPrice()));
        restaurant.setText(om.getMeal().getContainingRestaurant().getTitle());

        return convertView;
    }

}
