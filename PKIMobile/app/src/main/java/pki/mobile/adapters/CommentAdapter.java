package pki.mobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.time.format.DateTimeFormatter;
import java.util.List;

import pki.mobile.model.Comment;
import pki.mobile.pkimobile.R;

public class CommentAdapter extends ArrayAdapter<Comment> {


    public CommentAdapter(Context context, List<Comment> comments) {
        super(context, R.layout.graphic_comment, comments);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Comment comment = this.getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.graphic_comment, parent, false);
        }

        TextView text = convertView.findViewById(R.id.comment_text);
        TextView authorName = convertView.findViewById(R.id.comment_author_name);
        TextView date = convertView.findViewById(R.id.comment_date);

        text.setText(comment.getComment());
        authorName.setText(String.format("%s kaže:", comment.getUsername()));
        date.setText(comment.getMadeOn().format(DateTimeFormatter.ofPattern("dd.MM.yyyy. HH:mm:ss")));

        return convertView;
    }
}
