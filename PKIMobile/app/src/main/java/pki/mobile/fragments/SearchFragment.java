package pki.mobile.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import pki.mobile.activity.RestaurantActivity;
import pki.mobile.model.Category;
import pki.mobile.model.DB;
import pki.mobile.model.Restaurant;
import pki.mobile.model.Type;
import pki.mobile.pkimobile.R;

public class SearchFragment extends Fragment {

    private static final String KEY_CAN_ORDER = "CAN_ORDER";

    private AutoCompleteTextView title;
    private TextView meal;
    private TextView minAll, maxAll;
    private SeekBar seekBar;
    private Spinner type;
    private Spinner category;
    private Button searchButton;

    private int min, max;

    private boolean canOrder;
    private RecyclerView restaurantRecyclerView;
    private SearchAdapter adapter;

    public static SearchFragment newInstance(boolean canOrder) {
        SearchFragment fragment = new SearchFragment();
        Bundle b = new Bundle();
        b.putBoolean(KEY_CAN_ORDER, canOrder);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle b = getArguments();
        canOrder = b.getBoolean(KEY_CAN_ORDER);

        View view = inflater.inflate(R.layout.fragment_search, container, false);
        restaurantRecyclerView = view.findViewById(R.id.search_recycler_view);
        restaurantRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        restaurantRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));

        title = view.findViewById(R.id.search_title);
        meal = view.findViewById(R.id.search_meal);
        seekBar = view.findViewById(R.id.search_maxprice);
        type = view.findViewById(R.id.search_type);
        category = view.findViewById(R.id.search_category);
        minAll = view.findViewById(R.id.search_mintotal);
        maxAll = view.findViewById(R.id.search_maxtotal);
        title.setAdapter(new ArrayAdapter<>(
                getContext(), android.R.layout.simple_dropdown_item_1line, DB.restaurants));

        /*title.setOnItemClickListener((adapterView, simpleView, position, id) -> search());
        title.setOnFocusChangeListener((simpleView, hasFocus) -> {
            if (!hasFocus)
                search();
        });
        meal.setOnFocusChangeListener((simpleView, hasFocus) -> {
            if (!hasFocus)
                search();
        });*/

        type.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, Type.values()));
        category.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, Category.values()));
        /*type.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        search();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );
        */
        /*category.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        search();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );
        */
        /*seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                search();
            }
        });
        */

        min = DB.getCheapestMeal();
        max = DB.getPriciestMeal();

        minAll.setText(Integer.toString(min));
        maxAll.setText(Integer.toString(max));
        /*seekBar.setMin(min);
        seekBar.setMax(max);*/

        searchButton = view.findViewById(R.id.search_button);
        searchButton.setOnClickListener(v -> search());

        updateUI();

        return view;
    }

    private void search() {
        String searchTitle = title.getText().toString();
        String searchMealName = meal.getText().toString();
        Category searchCategoryO = (Category) category.getSelectedItem();
        Type searchTypeO = (Type) type.getSelectedItem();
        float searchOfHundred = seekBar.getProgress();
        int searchPrice = (int) (min + (max - min) * searchOfHundred / 100);
        List<Restaurant> l = new ArrayList<>(DB.restaurants);
        l.removeIf(r -> !r.searchRestaurantTitle(searchTitle));
        l.removeIf(r -> !r.searchMealName(searchMealName));
        l.removeIf(r -> !r.searchCategory(searchCategoryO));
        l.removeIf(r -> !r.searchType(searchTypeO));
        l.removeIf(r -> !r.searchPrice(searchPrice));

        changeUI(l);
    }

    private void updateUI() {
        if (adapter == null) {
            List<Restaurant> restaurants = DB.restaurants;
            adapter = new SearchAdapter(restaurants);
        }
        restaurantRecyclerView.setAdapter(adapter);
    }

    private void changeUI(List<Restaurant> restaurants) {
        restaurantRecyclerView.setAdapter(new SearchAdapter(restaurants));
        restaurantRecyclerView.getAdapter().notifyDataSetChanged();
    }

    private class SearchHolder extends RecyclerView.ViewHolder {

        private final TextView title;
        private final TextView category;
        private final TextView rating;
        private final TextView open;
        private final TextView minimum;

        public SearchHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.graphic_restaurant, parent, false));
            title = itemView.findViewById(R.id.search_list_item_restaurant_title);
            category = itemView.findViewById(R.id.search_list_item_restaurant_category);
            rating = itemView.findViewById(R.id.search_list_item_restaurant_rating);
            open = itemView.findViewById(R.id.search_list_item_restaurant_open);
            minimum = itemView.findViewById(R.id.search_list_item_restaurant_minimum);
            itemView.setOnClickListener(v -> {
                Restaurant restaurant = DB.getRestaurantFromTitle(title.getText().toString());
                Intent intent = RestaurantActivity.newInstance(getContext(), restaurant.getTitle(), canOrder);
                startActivity(intent);
            });
        }

        protected Restaurant.Hours getCurrentHours(Restaurant r) {
            DayOfWeek dow = DB.getDayOfWeek();
            int index = 0;
            if (dow == DayOfWeek.SATURDAY)
                index = 1;
            else if (dow == DayOfWeek.SUNDAY)
                index = 2;
            return r.getHours()[index];
        }

        private String getRestaurantStatus(Restaurant r) {
            LocalTime now = DB.getTime();
            Restaurant.Hours h = getCurrentHours(r);
            if ((now.isBefore(h.start)) || (now.isAfter(h.end))) {
                open.setTextColor(Color.RED);
                return "Zatvoreno!";
            }
            open.setTextColor(Color.GREEN);
            return "Otvoreno!";
        }

        public void bind(Restaurant restaurant) {
            title.setText(restaurant.getTitle());
            category.setText(restaurant.getCategoryFullName());
            rating.setText(String.format("%.2f", restaurant.calculateGrade()));
            open.setText(getRestaurantStatus(restaurant));
            minimum.setText(String.format("Minimalno:\n %d RSD", restaurant.getMinPrice()));
        }

    }

    private class SearchAdapter extends RecyclerView.Adapter<SearchHolder> {
        private final List<Restaurant> restaurants;


        public SearchAdapter(List<Restaurant> restaurants) {
            this.restaurants = restaurants;
        }

        @NonNull
        @Override
        public SearchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new SearchHolder(layoutInflater, parent);
        }

        @Override
        public void onBindViewHolder(@NonNull SearchHolder holder, int position) {
            Restaurant r = restaurants.get(position);
            holder.bind(r);
        }

        @Override
        public int getItemCount() {
            return restaurants.size();
        }
    }
}
