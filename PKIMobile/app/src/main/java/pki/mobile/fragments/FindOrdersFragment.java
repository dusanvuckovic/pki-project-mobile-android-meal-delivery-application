package pki.mobile.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import pki.mobile.activity.DeliveryActivity;
import pki.mobile.activity.ViewableOrderActivity;
import pki.mobile.adapters.DeliverableOrderAdapter;
import pki.mobile.model.DB;
import pki.mobile.model.Order;
import pki.mobile.pkimobile.R;

public class FindOrdersFragment extends Fragment {

    private static final int VIEWABLE_ORDER_ACTIVITY = 0;
    private ListView lv;
    private DeliverableOrderAdapter doa;

    public static FindOrdersFragment newInstance() {
        return new FindOrdersFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_find_orders, container, false);
        lv = v.findViewById(R.id.find_orders_listview);
        List<Order> orders = DB.orders.stream().filter(p -> p.getStatus() == Order.Status.MADE).collect(Collectors.toList());
        doa = new DeliverableOrderAdapter(getContext(), orders);
        lv.setAdapter(doa);
        lv.setEmptyView(v.findViewById(R.id.find_orders_empty));
        lv.setOnItemClickListener((parent, view, position, id) -> {
            Order o = (Order) parent.getAdapter().getItem(position);
            Intent i = ViewableOrderActivity.newInstance(getContext(), o.getID());
            startActivityForResult(i, VIEWABLE_ORDER_ACTIVITY);
        });
        return v;
    }

    public void update() {
        List<Order> list = DB.orders.stream().filter(p -> p.getStatus() == Order.Status.MADE).collect(Collectors.toList());
        if (list == null)
            list = new ArrayList<>();
        if (lv != null && getContext() != null) {
            doa = new DeliverableOrderAdapter(getContext(), list);
            lv.setAdapter(doa);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VIEWABLE_ORDER_ACTIVITY && resultCode == Activity.RESULT_OK) {
            DeliveryActivity da = (DeliveryActivity) getActivity();
            da.update();
        }
    }

}
