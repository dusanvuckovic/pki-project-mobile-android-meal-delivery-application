package pki.mobile.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

import pki.mobile.activity.DeliveryActivity;
import pki.mobile.helper.DirectionsHelper;
import pki.mobile.pkimobile.R;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MapViewFragment extends Fragment {


    private static final LatLng BELGRADE = new LatLng(44.787197, 20.457273);
    private static final int REQUEST_LOCATION_PERMISSIONS = 0;
    private SupportMapFragment map;
    private FusedLocationProviderClient fusedLocationProviderClient;

    public static MapViewFragment newInstance() {
        return new MapViewFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map_view, container, false);
        map = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map_view_map);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(getActivity(), permissions, 0);
        } else {
            map.getMapAsync(googleMap -> {
                googleMap.setMyLocationEnabled(true);
                fusedLocationProviderClient.getLastLocation().addOnSuccessListener(l -> {
                    LatLng ll = new LatLng(l.getLatitude(), l.getLongitude());
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ll, 15));
                });
            });

        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DeliveryActivity) getActivity()).update();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION_PERMISSIONS) {
            if (grantResults.length == 2 && grantResults[0] == PERMISSION_GRANTED && grantResults[1] == PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    map.getMapAsync(googleMap -> {
                        googleMap.setMyLocationEnabled(true);
                        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(
                                l -> googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(l.getLatitude(), l.getLongitude()), 15))
                        );
                    });

                }
            }
        }
    }

    public void update(Deque<LatLng> list) {
        if (map == null)
            return;
        List<DirectionsHelper.Route> routes = DirectionsHelper.getDirectionsHelper().getRoutes(new ArrayList<>(list));
        map.getMapAsync(googleMap -> {
            googleMap.clear();
            if (list.getFirst() != null) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(list.getFirst(), 15));
                googleMap.addMarker(new MarkerOptions().position(list.getFirst()).draggable(false).title("Ja"));
                for (DirectionsHelper.Route r : routes)
                    googleMap.addPolyline(new PolylineOptions().add(r.start).add(r.end).color(r.color));
            } else {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(BELGRADE, 15));
            }
        });
    }
}


