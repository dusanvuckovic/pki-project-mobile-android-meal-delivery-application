package pki.mobile.fragments.register_part_fragments;

import android.Manifest;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import pki.mobile.activity.CustomerActivity;
import pki.mobile.activity.DeliveryActivity;
import pki.mobile.activity.UnregisteredActivity;
import pki.mobile.helper.DirectionsHelper;
import pki.mobile.helper.MapGeocoder;
import pki.mobile.model.DB;
import pki.mobile.model.User;
import pki.mobile.pkimobile.R;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class RegisterFragment3Map extends Fragment {

    private static final String TAG = "RegisterFragment3Map";
    private static final String KEY_ADDRESS = "ADDRESS";
    private static final String KEY_LATITUDE = "LATITUDE";
    private static final String KEY_LONGITUDE = "LONGITUDE";
    private MapGeocoder mapGeocoder;
    private SupportMapFragment map;
    private EditText addressTextField;
    private Button currentLocationButton;
    private FusedLocationProviderClient fpc;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;

    public static RegisterFragment3Map newInstance() {
        return new RegisterFragment3Map();
    }

    public static RegisterFragment3Map newInstance(double latitude, double longitude, String address) {
        RegisterFragment3Map fragment = new RegisterFragment3Map();
        Bundle b = new Bundle();
        b.putDouble(KEY_LATITUDE, latitude);
        b.putDouble(KEY_LONGITUDE, longitude);
        b.putString(KEY_ADDRESS, address);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fpc = LocationServices.getFusedLocationProviderClient(getContext());

        View v = inflater.inflate(R.layout.fragment_register_3_map, container, false);
        addressTextField = v.findViewById(R.id.register_address_text);
        currentLocationButton = v.findViewById(R.id.register_current_location_button);
        Button finish = v.findViewById(R.id.buttonFG3);
        Button prev = v.findViewById(R.id.buttonBG3);

        finish.setOnClickListener(view -> {
            UnregisteredActivity activity = (UnregisteredActivity) getActivity();
            activity.updateMap(addressTextField.getText().toString(), mapGeocoder.getLatitude(), mapGeocoder.getLongitude());
            User user = activity.getUser();
            DB.loggedOn = user;
            DB.users.put(user.getUsername(), user);
            Class c = user.isDeliverer() ? DeliveryActivity.class : CustomerActivity.class;
            Intent i = new Intent(getActivity(), c);
            activity.startActivity(i);
            activity.finish();
        });
        prev.setOnClickListener(view -> {
            UnregisteredActivity ua = (UnregisteredActivity) getActivity();
            ua.updateMap(addressTextField.getText().toString(), mapGeocoder.getLatitude(), mapGeocoder.getLongitude());
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.register_child_fragment, ua.getRegisterFragment2Name());
            transaction.commit();
        });

        map = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.register_map);

        if (savedInstanceState != null && savedInstanceState.getString(KEY_ADDRESS) != null)
            restoreInstanceState(savedInstanceState);
        else if (this.getArguments() != null && this.getArguments().getString(KEY_ADDRESS) != null)
            restoreInstanceState(getArguments());
        else {
            mapGeocoder = new MapGeocoder(getContext(), map, true, addressTextField, currentLocationButton);
        }

        locationRequest = DirectionsHelper.createLocationRequest();

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PERMISSION_GRANTED) {
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(getActivity(), permissions, 0);
            return v;
        }
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location l = locationResult.getLastLocation();
                LatLng myLocation = new LatLng(l.getLatitude(), l.getLongitude());
                mapGeocoder.moveMap(myLocation, addressTextField);
                fpc.removeLocationUpdates(this);
            }
        };

        fpc.requestLocationUpdates(locationRequest, locationCallback, null);


        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getArguments() != null) {
            restoreInstanceState(getArguments());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        this.setArguments(prepareBundle(new Bundle()));
        UnregisteredActivity ua = (UnregisteredActivity) getActivity();
        ua.updateMap(addressTextField.getText().toString(), mapGeocoder.getLatitude(), mapGeocoder.getLongitude());
    }

    private void restoreInstanceState(Bundle inState) {
        addressTextField.setText(inState.getString(KEY_ADDRESS));
        double latitude = inState.getDouble(KEY_LATITUDE);
        double longitude = inState.getDouble(KEY_LONGITUDE);
        LatLng latLng = new LatLng(latitude, longitude);
        mapGeocoder = new MapGeocoder(getContext(), map, latLng, true, addressTextField, currentLocationButton);
    }

    private Bundle prepareBundle(Bundle outState) {
        outState.putDouble(KEY_LATITUDE, mapGeocoder.getLatitude());
        outState.putDouble(KEY_LONGITUDE, mapGeocoder.getLongitude());
        outState.putString(KEY_ADDRESS, addressTextField.getText().toString());
        return outState;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        prepareBundle(outState);
    }
}
