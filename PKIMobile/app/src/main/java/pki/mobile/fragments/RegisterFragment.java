package pki.mobile.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pki.mobile.activity.UnregisteredActivity;
import pki.mobile.pkimobile.R;

public class RegisterFragment extends Fragment {

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        UnregisteredActivity ua = (UnregisteredActivity) getActivity();
        View v = inflater.inflate(R.layout.fragment_register, container, false);
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.register_child_fragment, ua.getRegisterFragment1UserPass());
            transaction.commit();
        }
        return v;
    }
}
