package pki.mobile.fragments.register_part_fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;

import pki.mobile.activity.UnregisteredActivity;
import pki.mobile.model.DB;
import pki.mobile.pkimobile.R;

public class RegisterFragment1UserPass extends Fragment {

    private static final String KEY_USER = "USER";
    private static final String KEY_PASS = "PASS";
    private static final String KEY_TYPE = "TYPE";
    private EditText username, password;
    private RadioGroup type;
    private ImageButton eye;
    private Button next;

    public static RegisterFragment1UserPass newInstance(String username, String password, boolean isDeliverer) {
        RegisterFragment1UserPass fragment = new RegisterFragment1UserPass();
        Bundle b = new Bundle();
        b.putString(KEY_USER, username);
        b.putString(KEY_PASS, password);
        b.putInt(KEY_TYPE, isDeliverer ? R.id.radio_button_deliverer : R.id.radio_button_customer);
        fragment.setArguments(b);
        return fragment;
    }

    public static RegisterFragment1UserPass newInstance() {
        return new RegisterFragment1UserPass();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register_1_userpass, container, false);
        setRetainInstance(true);
        username = v.findViewById(R.id.register_username);
        password = v.findViewById(R.id.register_password);
        type = v.findViewById(R.id.register_radio_group);
        eye = v.findViewById(R.id.register_eye);
        eye.setOnClickListener(view -> {
            if (password.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
                password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
            else
                password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        });


        Button prev = v.findViewById(R.id.buttonBG1);
        next = v.findViewById(R.id.buttonFG1);

        next.setOnClickListener(view -> {
            next.requestFocus();
            if (isErrors()) {
                return;
            }
            UnregisteredActivity ua = (UnregisteredActivity) getActivity();
            ua.updateUserPass(username.getText().toString(), password.getText().toString(), type.getCheckedRadioButtonId());
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.register_child_fragment, ua.getRegisterFragment2Name());
            transaction.commit();
        });
        prev.setEnabled(false);

        username.setOnFocusChangeListener((view, hasFocus) -> {
            if (hasFocus)
                return;
            String text = username.getText().toString();
            if (text.isEmpty())
                username.setError("Morate uneti ime!");
            else if (DB.users.containsKey(text))
                username.setError("Ime je zauzeto!");
        });

        password.setOnFocusChangeListener((view, hasFocus) -> {
            if (hasFocus)
                return;
            String text = password.getText().toString();
            if (text.isEmpty()) {
                password.setError("Morate uneti lozinku!");
                setMarginsOnError(eye, true);
            } else {
                password.setError(null);
                setMarginsOnError(eye, false);
            }
        });

        if (savedInstanceState != null) {
            resumeInstanceState(savedInstanceState);
        } else if (this.getArguments() != null)
            resumeInstanceState(this.getArguments());

        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
        UnregisteredActivity ua = (UnregisteredActivity) getActivity();
        ua.updateUserPass(username.getText().toString(), password.getText().toString(), type.getCheckedRadioButtonId());
    }

    private void resumeInstanceState(@NonNull Bundle savedInstanceState) {
        String user = savedInstanceState.getString(KEY_USER);
        String pass = savedInstanceState.getString(KEY_PASS);
        int typeID = savedInstanceState.getInt(KEY_TYPE);

        resumeInstanceState(user, pass, typeID);

    }

    private void resumeInstanceState(String user, String pass, int typeID) {
        username.setText(user);
        password.setText(pass);
        type.check(typeID);
    }

    private void checkErrors() {
        String text = username.getText().toString();
        if (text.isEmpty())
            username.setError("Morate uneti ime!");
        else if (DB.users.containsKey(text))
            username.setError("Ime je zauzeto!");

        text = password.getText().toString();
        if (text.isEmpty()) {
            password.setError("Morate uneti lozinku!");
            setMarginsOnError(eye, true);
        } else {
            password.setError(null);
            setMarginsOnError(eye, false);
        }


    }

    private void setMarginsOnError(ImageButton eye, boolean isError) {
        ConstraintLayout.LayoutParams par = (ConstraintLayout.LayoutParams) eye.getLayoutParams();
        par.setMargins(par.leftMargin, par.topMargin, isError ? 100 : 16, par.bottomMargin);
    }

    private boolean isErrors() {
        checkErrors();
        boolean t = username.getError() != null;
        boolean p = password.getError() != null;
        return t || p;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(KEY_USER, username.getText().toString());
        outState.putString(KEY_PASS, password.getText().toString());
        outState.putInt(KEY_TYPE, type.getCheckedRadioButtonId());
    }
}
