package pki.mobile.fragments.restaurant_part_fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.util.Linkify;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import pki.mobile.model.DB;
import pki.mobile.model.Restaurant;
import pki.mobile.pkimobile.R;

public class RestaurantFragment3Contact extends Fragment {

    private static final String KEY_RESTAURANT_NAME = "RESTAURANT_NAME";

    private TextView workday;
    private TextView saturday;
    private TextView sunday;
    private TextView contact;
    private TextView address;
    private SupportMapFragment map;

    private Restaurant restaurant;

    public static RestaurantFragment3Contact newInstance(String restaurantName) {
        RestaurantFragment3Contact fragment = new RestaurantFragment3Contact();
        Bundle b = new Bundle();
        b.putString(KEY_RESTAURANT_NAME, restaurantName);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_restaurant_3_contact, container, false);
        String restaurantName = getArguments().getString(KEY_RESTAURANT_NAME);
        restaurant = DB.getRestaurantFromTitle(restaurantName);

        workday = v.findViewById(R.id.restaurant_contact_workday);
        saturday = v.findViewById(R.id.restaurant_contact_saturday);
        sunday = v.findViewById(R.id.restaurant_contact_sunday);
        contact = v.findViewById(R.id.restaurant_contact_phone);
        address = v.findViewById(R.id.restaurant_contact_address);
        map = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.restaurant_contact_map);

        workday.setText("Pon-pet: " + restaurant.getWorkdayTime());
        saturday.setText("Subota: " + restaurant.getSaturdayTime());
        sunday.setText("Nedelja: " + restaurant.getSundayTime());
        contact.setText(getPhones(restaurant));
        Linkify.addLinks(contact, Patterns.PHONE, "tel:");
        address.setText(getAddress(restaurant));
        map.getMapAsync(googleMap -> {
            LatLng pos = new LatLng(restaurant.getLatitude(), restaurant.getLongitude());
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 15));
            googleMap.addMarker(new MarkerOptions().position(pos).draggable(false).title(restaurantName));
        });

        return v;
    }

    private String getPhones(Restaurant r) {
        StringBuilder adr = new StringBuilder("Kontakt:");
        for (String s : r.getPhones())
            adr.append("\n").append(s);
        return adr.toString();
    }

    private String getAddress(Restaurant r) {
        return "Adresa:\n" + r.getAddress();
    }
}
