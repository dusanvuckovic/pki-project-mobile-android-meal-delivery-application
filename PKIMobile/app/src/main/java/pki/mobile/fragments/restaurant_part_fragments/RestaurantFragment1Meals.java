package pki.mobile.fragments.restaurant_part_fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import pki.mobile.activity.MealActivity;
import pki.mobile.adapters.ExpandableMealAdapter;
import pki.mobile.model.DB;
import pki.mobile.model.Meal;
import pki.mobile.model.Restaurant;
import pki.mobile.pkimobile.R;

public class RestaurantFragment1Meals extends Fragment {

    private static final String KEY_RESTAURANT_NAME = "RESTAURANT_NAME";
    private static final String KEY_CAN_ORDER = "CAN_ORDER";
    private ExpandableListView expandableListView;
    private ExpandableMealAdapter expandableMealAdapter;

    public static RestaurantFragment1Meals newInstance(String restaurantName, boolean canOrder) {
        RestaurantFragment1Meals fragment = new RestaurantFragment1Meals();
        Bundle b = new Bundle();
        b.putString(KEY_RESTAURANT_NAME, restaurantName);
        b.putBoolean(KEY_CAN_ORDER, canOrder);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_restaurant_1_meals, container, false);
        expandableListView = v.findViewById(R.id.simpleExpandableListView);
        Bundle b = getArguments();
        String restaurantName = b.getString(KEY_RESTAURANT_NAME);
        boolean canOrder = b.getBoolean(KEY_CAN_ORDER);
        if (restaurantName != null) {
            Restaurant r = DB.getRestaurantFromTitle(restaurantName);
            expandableMealAdapter = new ExpandableMealAdapter(r.getMeals(), getContext());
        } else {
            expandableMealAdapter = new ExpandableMealAdapter(DB.restaurants.get(0).getMeals(), getContext());
        }
        expandableListView.setAdapter(expandableMealAdapter);
        expandableListView.setOnChildClickListener((parent, view, groupPosition, childPosition, id) -> {
            Meal m = (Meal) expandableMealAdapter.getChild(groupPosition, childPosition);
            Intent intent = MealActivity.newInstance(getContext(), m.getContainingRestaurant().getTitle(), m.getName(), canOrder);
            startActivity(intent);
            return false;
        });
        return v;
    }
}
