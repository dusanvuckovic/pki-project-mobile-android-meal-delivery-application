package pki.mobile.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.List;
import java.util.stream.Collectors;

import pki.mobile.adapters.FinishedOrderAdapter;
import pki.mobile.model.DB;
import pki.mobile.model.Order;
import pki.mobile.pkimobile.R;

public class AllOrdersFragment extends Fragment {

    private ListView lv;

    public static AllOrdersFragment newInstance() {
        return new AllOrdersFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_all_orders, container, false);
        lv = v.findViewById(R.id.all_orders_listview);
        lv.setAdapter(new FinishedOrderAdapter(getContext(), getOrders()));
        lv.setEmptyView(v.findViewById(R.id.all_orders_empty));
        return v;
    }

    public void update() {
        if (lv != null) {
            lv.setAdapter(new FinishedOrderAdapter(getContext(), getOrders()));
        }
    }

    private List<Order> getOrders() {
        return DB.orders.stream().filter(p -> p.getUserWhoOrdered() == DB.loggedOn).collect(Collectors.toList());
    }
}
