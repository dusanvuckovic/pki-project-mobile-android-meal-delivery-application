package pki.mobile.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pki.mobile.activity.CustomerActivity;
import pki.mobile.adapters.OrderedMealAdapter;
import pki.mobile.helper.DirectionsHelper;
import pki.mobile.helper.MapGeocoder;
import pki.mobile.model.DB;
import pki.mobile.model.Order;
import pki.mobile.model.OrderedMeal;
import pki.mobile.model.Restaurant;
import pki.mobile.pkimobile.R;

public class CurrentOrderFragment extends Fragment {

    private static final String KEY_ADDRESS = "ADDRESS";
    private static final String KEY_LATITUDE = "LATITUDE";
    private static final String KEY_LONGITUDE = "LONGITUDE";
    private TextView amount;
    private ListView listView;
    private EditText address;
    private MapGeocoder mg;
    private LatLng defaultLatLng;
    private LatLng userLatLng;
    private Button finish;

    public static CurrentOrderFragment newInstance() {
        return new CurrentOrderFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_current_order, container, false);

        defaultLatLng = new LatLng(DB.loggedOn.getLatitude(), DB.loggedOn.getLongitude());

        listView = v.findViewById(R.id.current_order_listview);
        //ViewGroup parentGroup = (ViewGroup) listView.getParent();
        //View empty = getActivity().getLayoutInflater().inflate(R.layout.empty_view, null,false);
        //parentGroup.addView(empty);
        Button changeAddress = v.findViewById(R.id.current_order_change_address);
        Button defaultAddress = v.findViewById(R.id.current_order_default_address);
        finish = v.findViewById(R.id.current_order_finish);
        address = v.findViewById(R.id.current_order_address);
        amount = v.findViewById(R.id.current_order_amount);
        setAmount();

        SupportMapFragment map = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.current_order_map);

        listView.setAdapter(new OrderedMealAdapter(getContext(), DB.checkout, this));
        listView.setEmptyView(v.findViewById(R.id.current_order_empty));

        if (savedInstanceState == null) {
            userLatLng = defaultLatLng;
            mg = new MapGeocoder(getContext(), map, userLatLng,
                    false, address, changeAddress);
        } else {
            double lat = savedInstanceState.getDouble(KEY_LATITUDE);
            double lng = savedInstanceState.getDouble(KEY_LONGITUDE);
            String startAddress = savedInstanceState.getString(KEY_ADDRESS);

            userLatLng = new LatLng(lat, lng);
            mg = new MapGeocoder(getContext(), map, userLatLng, startAddress, false, address, changeAddress);
        }

        defaultAddress.setOnClickListener(view -> {
            address.setText(DB.loggedOn.getAddress());
            mg.moveMap(defaultLatLng);
        });

        finish.setOnClickListener(view -> {
            DirectionsHelper d = pki.mobile.helper.DirectionsHelper.getDirectionsHelper();
            Map<Restaurant, List<OrderedMeal>> restaurantMap = new HashMap();
            for (OrderedMeal meal : DB.checkout) {
                Restaurant r = meal.getMeal().getContainingRestaurant();
                if (restaurantMap.containsKey(r))
                    restaurantMap.get(r).add(meal);
                else {
                    List<OrderedMeal> mealList = new ArrayList<>();
                    mealList.add(meal);
                    restaurantMap.put(r, mealList);
                }
            }
            for (Restaurant key : restaurantMap.keySet()) {
                Order o = new Order(DB.loggedOn, key, LocalDateTime.now(), "");
                LatLng mgLatLng = mg.getLatLng();
                double restaurantToUserDistance = (address.getText().toString().equals(DB.loggedOn.getAddress()))
                        ? d.getDistanceInMeters(key.getLatLng(), DB.loggedOn.getLatLng())
                        : d.getDistanceInMeters(key.getLatLng(), mgLatLng);
                if (!address.getText().toString().equals(DB.loggedOn.getAddress())) {
                    o.setDeliveredTo(address.getText().toString());
                    o.setDeliveryLatLng(mgLatLng);
                }
                o.setRestaurantToUserKm(restaurantToUserDistance);
                o.setMeals(restaurantMap.get(key));
                o.setStatus(Order.Status.MADE);
                DB.orders.add(o);

            }

            DB.checkout.clear();
            CustomerActivity ca = (CustomerActivity) getActivity();
            ca.update();
            ca.setTab(CustomerActivity.ALL_ORDERS_FRAGMENT);
        });

        checkDisable();

        return v;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putDouble(KEY_LATITUDE, mg.getLatitude());
        outState.putDouble(KEY_LONGITUDE, mg.getLongitude());
        outState.putString(KEY_ADDRESS, address.getText().toString());
    }

    public void setAmount() {
        if (amount != null)
            amount.setText(String.format("Račun: %d RSD", OrderedMeal.getMealPrice(DB.checkout)));
    }

    public void update() {
        if (listView != null) {
            OrderedMealAdapter adapter = (OrderedMealAdapter) listView.getAdapter();
            if (adapter != null)
                adapter.notifyDataSetChanged();
        }
        setAmount();
        checkDisable();
    }


    public void checkDisable() {
        if (listView != null && finish != null)
            finish.setEnabled(!listView.getAdapter().isEmpty());
    }
}
