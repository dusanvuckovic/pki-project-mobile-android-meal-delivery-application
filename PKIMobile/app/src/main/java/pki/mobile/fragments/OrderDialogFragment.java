package pki.mobile.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.NumberKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import pki.mobile.activity.MealActivity;
import pki.mobile.model.DB;
import pki.mobile.model.Meal;
import pki.mobile.model.OrderedMeal;
import pki.mobile.model.Restaurant;
import pki.mobile.pkimobile.R;

public class OrderDialogFragment extends DialogFragment {

    private static final String KEY_RESTAURANT_TITLE = "RESTAURANT_TITLE";
    private static final String KEY_MEAL_NAME = "MEAL_NAME";
    private ImageView minus;
    private ImageView plus;
    private EditText number;
    private Meal meal;
    private MealActivity listener;

    public static OrderDialogFragment newInstance(String restaurantTitle, String mealName) {
        Bundle b = new Bundle();
        b.putString(KEY_RESTAURANT_TITLE, restaurantTitle);
        b.putString(KEY_MEAL_NAME, mealName);
        OrderDialogFragment odf = new OrderDialogFragment();
        odf.setArguments(b);
        return odf;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (MealActivity) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String restaurantName = getArguments().getString(KEY_RESTAURANT_TITLE);
        String mealName = getArguments().getString(KEY_MEAL_NAME);

        Restaurant r = DB.getRestaurantFromTitle(restaurantName);
        meal = r.getMealByName(mealName);

        View v = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_order, null);
        minus = v.findViewById(R.id.dialog_minus);
        plus = v.findViewById(R.id.dialog_plus);
        number = v.findViewById(R.id.dialog_num);

        minus.setOnClickListener(view -> {
            int num = getNumber();
            if (num > 0)
                setNumber(num - 1);
        });
        plus.setOnClickListener(view -> setNumber(getNumber() + 1));
        number.getText().setFilters(new InputFilter[]{new NumberKeyListener() {
            @Override
            protected char[] getAcceptedChars() {
                return "1234567890,".toCharArray();
            }

            @Override
            public int getInputType() {
                return InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
            }
        }});

        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(meal.getName())
                .setPositiveButton("Prihvati", (dialog, which) -> {
                    DB.checkout.add(new OrderedMeal(meal, getNumber()));
                    listener.onDialogPositiveClick(this);
                })
                .setNegativeButton("Odbij", (dialog, which) -> {
                })
                .create();
    }

    private int getNumber() {
        return Integer.parseInt(number.getText().toString());
    }

    private void setNumber(int num) {
        number.setText(Integer.toString(num));
    }

    public interface NoticeDialogListener {
        void onDialogPositiveClick(DialogFragment dialog);

        void onDialogNegativeClick(DialogFragment dialog);
    }
}
