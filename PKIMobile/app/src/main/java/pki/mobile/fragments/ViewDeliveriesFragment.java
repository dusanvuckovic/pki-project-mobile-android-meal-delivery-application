package pki.mobile.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pki.mobile.activity.DeliveryActivity;
import pki.mobile.activity.OrderActivity;
import pki.mobile.helper.ItemTouchHelperAdapter;
import pki.mobile.model.DB;
import pki.mobile.model.Order;
import pki.mobile.pkimobile.R;

public class ViewDeliveriesFragment extends Fragment {

    private RecyclerView recyclerView;
    private SelectedRestaurantAdapter sra;
    private ItemTouchHelper touchHelper;
    private ItemTouchHelper.Callback callback;

    public static ViewDeliveriesFragment newInstance() {
        return new ViewDeliveriesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<Order> orders = DB.getAcceptedOrders();
        sra = new SelectedRestaurantAdapter(orders);
        callback = new SimpleItemTouchHelperCallback(sra);
        touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_view_deliveries, container, false);
        recyclerView = v.findViewById(R.id.view_deliveries_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(sra);
        touchHelper.attachToRecyclerView(recyclerView);
        return v;
    }

    public void update() {
        List<Order> orders = DB.getAcceptedOrders();
        if (sra != null)
            sra.putOrders(orders);
    }

    public boolean hasOrderPositions() {
        return sra != null && sra.orders != null;
    }

    public List<LatLng> getOrderPositions() {
        List<LatLng> list = new ArrayList<>();
        for (Order o : sra.orders) {
            list.add(o.getRestaurantDelivering().getLatLng());
            list.add(o.getDeliveryLatLng());
        }
        return list;
    }

    public class SelectedRestaurantHolder extends RecyclerView.ViewHolder {

        private final TextView username;
        private final TextView price;
        private final TextView restaurantName;
        private final TextView deliverableTime;
        private final TextView userAddress;
        private final TextView restaurantAddress;
        private final TextView id;
        private final TextView km;


        private final ImageView checkmark;
        private final ImageView orderImage;

        private Order o;

        public SelectedRestaurantHolder(View v) {
            super(v);

            username = itemView.findViewById(R.id.graphic_deliverable_username);
            price = itemView.findViewById(R.id.graphic_deliverable_price);
            restaurantName = itemView.findViewById(R.id.graphic_deliverable_name);
            deliverableTime = itemView.findViewById(R.id.graphic_deliverable_time);
            userAddress = itemView.findViewById(R.id.graphic_deliverable_user_address);
            restaurantAddress = itemView.findViewById(R.id.graphic_deliverable_restaurant_status);
            id = itemView.findViewById(R.id.graphic_deliverable_id);
            km = itemView.findViewById(R.id.graphic_deliverable_km);

            checkmark = itemView.findViewById(R.id.graphic_deliverable_checkmark);
            orderImage = itemView.findViewById(R.id.graphic_deliverable_image);

            itemView.setOnClickListener(view -> {
                Intent orderActivity = OrderActivity.newInstance(getContext(), o.getID());
                startActivity(orderActivity);
            });

        }

        public void bind(Order o) {
            this.o = o;

            username.setText(String.format("%s (%s)", o.getUserWhoOrdered().getFullName(), o.getRestaurantToUserDistanceKM()));
            userAddress.setText(o.getUserWhoOrdered().getAddress());

            restaurantName.setText(String.format("%s (%s)", o.getRestaurantDelivering().getTitle(), o.getDelivererToRestaurantDistanceKM()));
            restaurantAddress.setText(o.getRestaurantDelivering().getAddress());

            price.setText(String.format("%d RSD", o.calculatePrice()));
            deliverableTime.setText(o.getTimeFromStatus());
            if (o.getRestaurantToUserDistance() != null && o.getDelivererToRestaurantDistance() != null)
                km.setText(String.format("%.2f km", o.getRestaurantToUserDistance() + o.getDelivererToRestaurantDistance()));
            else
                km.setText("računamo...");
            id.setText(String.format("Narudžbina #%d", o.getID()));

            if (!o.getRestaurantDelivering().getImages().isEmpty()) {
                orderImage.setImageResource(o.getRestaurantDelivering().getImages().get(0));
            }
            checkmark.setVisibility(View.GONE);

        }
    }

    private class SelectedRestaurantAdapter extends RecyclerView.Adapter<SelectedRestaurantHolder>
            implements ItemTouchHelperAdapter {

        private List<Order> orders;

        public SelectedRestaurantAdapter(List<Order> orders) {
            this.orders = orders;
        }

        public void putOrders(List<Order> orders) {
            this.orders = orders;
            notifyDataSetChanged();
        }

        public List<Order> getOrders() {
            return orders;
        }

        @NonNull
        @Override
        public SelectedRestaurantHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.graphic_deliverable_order, parent, false);
            return new SelectedRestaurantHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull SelectedRestaurantHolder holder, int position) {
            Order o = orders.get(position);
            holder.bind(o);
        }

        @Override
        public int getItemCount() {
            return orders.size();
        }

        @Override
        public void onItemDismiss(int position) {
            Order o = orders.get(position);
            o.setStatus(Order.Status.FAILED);
            o.setDeliveredOrDeniedOn(LocalDateTime.now());
            orders.remove(position);
            notifyItemRemoved(position);
            ((DeliveryActivity) getActivity()).updateMap();
        }

        @Override
        public void onItemMove(int fromPosition, int toPosition) {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(orders, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(orders, i, i - 1);
                }
            }
            notifyItemMoved(fromPosition, toPosition);
            ((DeliveryActivity) getActivity()).updateMap();
        }
    }

    public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

        private final SelectedRestaurantAdapter adapter;

        public SimpleItemTouchHelperCallback(SelectedRestaurantAdapter adapter) {
            this.adapter = adapter;
        }

        @Override
        public boolean isLongPressDragEnabled() {
            return true;
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return true;
        }

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
            int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
            return makeMovementFlags(dragFlags, swipeFlags);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                              RecyclerView.ViewHolder target) {
            adapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            adapter.onItemDismiss(viewHolder.getAdapterPosition());
        }
    }
}
