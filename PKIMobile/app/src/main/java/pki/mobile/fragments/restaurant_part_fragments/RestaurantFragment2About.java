package pki.mobile.fragments.restaurant_part_fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.time.LocalDateTime;

import pki.mobile.adapters.CommentAdapter;
import pki.mobile.adapters.RestaurantImageAdapter;
import pki.mobile.model.Comment;
import pki.mobile.model.DB;
import pki.mobile.model.Restaurant;
import pki.mobile.pkimobile.R;

public class RestaurantFragment2About extends Fragment {

    private static final String KEY_RESTAURANT_NAME = "RESTAURANT_NAME";

    private Restaurant restaurant;

    private TextView description;
    private GridView gridView;
    private ListView listView;
    private EditText commentBox;
    private RatingBar rating;
    private Button button;

    public static RestaurantFragment2About newInstance(String restaurantName) {
        RestaurantFragment2About fragment = new RestaurantFragment2About();
        Bundle b = new Bundle();
        b.putString(KEY_RESTAURANT_NAME, restaurantName);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_restaurant_2_about, container, false);

        String restaurantName = getArguments().getString(KEY_RESTAURANT_NAME);
        restaurant = DB.getRestaurantFromTitle(restaurantName);

        description = v.findViewById(R.id.restaurant_about_description);
        gridView = v.findViewById(R.id.restaurant_about_grid_view);
        listView = v.findViewById(R.id.restaurant_about_list_view);
        commentBox = v.findViewById(R.id.restaurant_about_comment_text);
        rating = v.findViewById(R.id.restaurant_about_rating);
        button = v.findViewById(R.id.restaurant_about_rate_and_comment);

        description.setText(restaurant.getDescription());
        gridView.setAdapter(new RestaurantImageAdapter(getContext(), restaurant.getImages()));
        listView.setAdapter(new CommentAdapter(getContext(), restaurant.getComments()));

        button.setOnClickListener(view -> {
            boolean shouldDisable = false;
            String text = commentBox.getText().toString().trim();
            double grade = rating.getRating();
            if (!text.isEmpty()) {
                shouldDisable = true;
                Comment c = new Comment(text, DB.loggedOn.getUsername(), LocalDateTime.now());
                restaurant.getComments().add(c);
                ((BaseAdapter) listView.getAdapter()).notifyDataSetChanged();
            }
            if (grade != 0) {
                shouldDisable = true;
                restaurant.getGrades().add((int) grade);
            }
            if (shouldDisable) {
                button.setEnabled(false);
                button.setFocusable(false);
                rating.setFocusable(false);
                rating.setEnabled(false);
                commentBox.setEnabled(false);
                commentBox.setFocusable(false);
                commentBox.getText().clear();
            }

        });
        if (!DB.isLoggedIn()) {
            button.setEnabled(false);
            rating.setEnabled(false);
            commentBox.setEnabled(false);
            commentBox.setFocusable(false);
        }


        return v;
    }


}
