package pki.mobile.fragments.register_part_fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import pki.mobile.activity.UnregisteredActivity;
import pki.mobile.pkimobile.R;

public class RegisterFragment2Name extends Fragment {

    private static final String KEY_FIRST_NAME = "FIRST_NAME";
    private static final String KEY_LAST_NAME = "LAST_NAME";
    private static final String KEY_PHONE = "PHONE";
    private TextView firstName;
    private TextView lastName;
    private TextView phone;

    public RegisterFragment2Name() {
        this.setRetainInstance(true);
    }

    public static RegisterFragment2Name newInstance() {
        return new RegisterFragment2Name();
    }

    public static RegisterFragment2Name newInstance(String firstName, String lastName, String phone) {
        RegisterFragment2Name fragment = new RegisterFragment2Name();
        Bundle b = new Bundle();
        b.putString(KEY_FIRST_NAME, firstName);
        b.putString(KEY_LAST_NAME, lastName);
        b.putString(KEY_PHONE, phone);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register_2_name, container, false);
        Button next = v.findViewById(R.id.buttonFG2);
        Button prev = v.findViewById(R.id.buttonBG2);
        firstName = v.findViewById(R.id.register_firstname);
        lastName = v.findViewById(R.id.register_lastname);
        phone = v.findViewById(R.id.register_phone);

        next.setOnClickListener(view -> {
            if (isErrors()) {
                setErrors();
                return;
            }
            UnregisteredActivity ua = (UnregisteredActivity) getActivity();
            ua.updateName(firstName.getText().toString(), lastName.getText().toString(), phone.getText().toString());
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.register_child_fragment, ua.getRegisterFragment3Map());
            transaction.commit();
        });

        prev.setOnClickListener(view -> {
            clearErrors();
            UnregisteredActivity ua = (UnregisteredActivity) getActivity();
            ua.updateName(firstName.getText().toString(), lastName.getText().toString(), phone.getText().toString());
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.register_child_fragment, ua.getRegisterFragment1UserPass());
            transaction.commit();
        });

        if (savedInstanceState != null)
            restoreInstanceState(savedInstanceState);
        if (getArguments() != null)
            restoreInstanceState(getArguments());

        return v;
    }

    @Override
    public void onPause() {
        super.onPause();
        UnregisteredActivity ua = (UnregisteredActivity) getActivity();
        ua.updateName(firstName.getText().toString(), lastName.getText().toString(), phone.getText().toString());
    }

    private void restoreInstanceState(@NonNull Bundle inState) {
        firstName.setText(inState.getString(KEY_FIRST_NAME));
        lastName.setText(inState.getString(KEY_LAST_NAME));
        phone.setText(inState.getString(KEY_PHONE));
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString(KEY_FIRST_NAME, firstName.getText().toString());
        outState.putString(KEY_LAST_NAME, lastName.getText().toString());
        outState.putString(KEY_PHONE, phone.getText().toString());
    }

    private boolean isErrors() {
        boolean f = firstName.getText().toString().isEmpty();
        boolean l = lastName.getText().toString().isEmpty();
        boolean p = phone.getText().toString().isEmpty();
        return f || l || p;
    }

    private void setErrors() {
        if (firstName.getText().toString().isEmpty())
            firstName.setError("Morate uneti ime!");
        if (lastName.getText().toString().isEmpty())
            lastName.setError("Morate uneti prezime!");
        if (phone.getText().toString().isEmpty())
            phone.setError("Morate uneti telefon!");
    }

    private void clearErrors() {
        firstName.setError(null);
        lastName.setError(null);
        phone.setError(null);
    }

}
