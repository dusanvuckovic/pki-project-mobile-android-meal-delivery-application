package pki.mobile.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import pki.mobile.activity.CustomerActivity;
import pki.mobile.activity.DeliveryActivity;
import pki.mobile.helper.Helper;
import pki.mobile.model.DB;
import pki.mobile.model.User;
import pki.mobile.pkimobile.R;

public class LoginFragment extends Fragment {


    private EditText username, password;
    private Button login;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_login, container, false);
        username = v.findViewById(R.id.login_user);
        password = v.findViewById(R.id.login_pass);
        login = v.findViewById(R.id.login_button);
        login.setOnClickListener(this::tryLogin);
        ImageButton eye = v.findViewById(R.id.login_eye);
        eye.setOnClickListener(view -> {
            if (password.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
                password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
            else
                password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        });
        return v;
    }

    private void tryLogin() {
        String username = this.username.getText().toString();
        String password = this.password.getText().toString();

        if ("".equals(username) || "".equals(password)) {
            makeSnackbar("Morate popuniti sva polja!");
            return;
        }

        if (DB.users.containsKey(username)) {
            User user = DB.users.get(username);
            if (user.getPassword().equals(password)) {
                DB.loggedOn = user;
                makeSnackbar("Uspešan login!");
                Class<?> classT;
                classT = user.isDeliverer() ? DeliveryActivity.class : CustomerActivity.class;
                Intent intent = new Intent(getActivity(), classT);
                getActivity().startActivity(intent);
                getActivity().finish();
            } else {
                makeSnackbar("Pogrešna lozinka!");
            }

        } else {
            makeSnackbar("Nema takvog korisničkog imena!");
        }
    }

    private void tryLogin(View view) {
        tryLogin();
    }

    private void makeSnackbar(String text) {
        Helper.makeSnackbar(getActivity(), text);
    }
}
