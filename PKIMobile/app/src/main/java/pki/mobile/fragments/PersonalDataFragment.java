package pki.mobile.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import pki.mobile.activity.CustomerActivity;
import pki.mobile.activity.NavigableActivity;
import pki.mobile.helper.MapGeocoder;
import pki.mobile.model.DB;
import pki.mobile.model.User;
import pki.mobile.pkimobile.R;

public class PersonalDataFragment extends Fragment {

    private EditText user;
    private EditText password;
    private EditText firstName;
    private EditText lastName;
    private EditText phone;
    private EditText address;
    private ImageView image;
    private Button button;
    private SupportMapFragment map;
    private MapGeocoder mapGeocoder;

    public static PersonalDataFragment newInstance() {
        return new PersonalDataFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_personal_data, container, false);

        user = v.findViewById(R.id.personal_data_user);
        password = v.findViewById(R.id.personal_data_pass);
        firstName = v.findViewById(R.id.personal_data_first_name);
        lastName = v.findViewById(R.id.personal_data_last_name);
        phone = v.findViewById(R.id.personal_data_phone);
        address = v.findViewById(R.id.personal_data_address);
        image = v.findViewById(R.id.personal_data_image);
        button = v.findViewById(R.id.personal_data_button);
        map = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.personal_data_map);

        setDataFromUser(DB.loggedOn);

        mapGeocoder = new MapGeocoder(getContext(), map,
                new LatLng(DB.loggedOn.getLatitude(), DB.loggedOn.getLongitude()), false,
                address, image);

        user.setEnabled(false);
        user.setFocusable(false);

        button.setOnClickListener(view -> {
            changeCurrentData(DB.loggedOn);
            NavigableActivity na = (NavigableActivity) getActivity();
            na.setTab(CustomerActivity.SEARCH_FRAGMENT);
        });

        ImageButton eye = v.findViewById(R.id.personal_data_eye);
        eye.setOnClickListener(view -> {
            if (password.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
                password.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD | InputType.TYPE_CLASS_TEXT);
            else
                password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        });
        return v;
    }

    private void setDataFromUser(User u) {
        user.setText(u.getUsername());
        password.setText(u.getPassword());
        firstName.setText(u.getFirstName());
        lastName.setText(u.getLastName());
        phone.setText(u.getPhone());
        address.setText(u.getAddress());
    }

    private void changeCurrentData(User u) {
        u.setPassword(password.getText().toString());
        u.setFirstName(firstName.getText().toString());
        u.setLastName(lastName.getText().toString());
        u.setPhone(phone.getText().toString());
        u.setAddress(address.getText().toString());
        u.setLatitude(mapGeocoder.getLatitude());
        u.setLongitude(mapGeocoder.getLongitude());
    }

}
