package pki.mobile.model;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pki.mobile.pkimobile.R;

public class DB {

    public static final int NO_IMAGE = R.drawable.noimage;
    public static final List<Restaurant> restaurants = new LinkedList<>();
    public static final List<Order> orders = new LinkedList<>();
    public static final Map<String, User> users = new HashMap<>();
    public static final List<OrderedMeal> checkout = new LinkedList<>();
    private static final User NOT_LOGGED_ON = new User("", "", "", "", 0, 0, "", "", false);
    private static final Restaurant NO_RESTAURANT = new Restaurant("", "", Category.NONE, "", 0, 0, 0, 0);
    private static final Meal NO_MEAL = new Meal("", Category.NONE, Type.NONE, "", 0, null);
    private static final User mi;
    private static final User ni;
    public static User loggedOn = NOT_LOGGED_ON;
    public static Restaurant selectedRestaurant = NO_RESTAURANT;
    public static Meal selectedMeal = NO_MEAL;

    static {
        mi = new User("Miloš", "Petrović",
                "065456789", "Kralja Petra 13/2", 44.818896, 20.453583,
                "mi", "im", false);
        ni = new User("Nikola", "Jovanović",
                "0611144254", "Džordža Vašingtona 56", 44.813959, 20.471801,
                "ni", "in", true);
        users.put(mi.getUsername(), mi);
        users.put(ni.getUsername(), ni);

        /* BOTAKO */
        Restaurant botako = new Restaurant(
                "Botako",
                "Nevesinjska 6",
                Category.ITALIAN_FOOD,
                "Botako od 2004. godine spaja tradiciju " +
                        "italijanske kuhinje sa domaćom tradicijom. Uz autentičan ambijent, na Botako meniu " +
                        "se nalaze italijanske pice, lazanje, paste, poslastice, brusketi i salate, kao i " +
                        "nekoliko varijacija doručka. U novembru 2013. je otvoren još jedan Botako restoran, " +
                        "na atraktivnoj lokaciji - blizu centra - Šantićeva br.8.",
                1000,
                45,
                44.799454,
                20.473338
        );
        botako.addHours(
                Restaurant.Day.WORKDAY,
                LocalTime.of(10, 0),
                LocalTime.of(20, 30)
        );
        botako.addHours(
                Restaurant.Day.SATURDAY,
                LocalTime.of(11, 0),
                LocalTime.of(22, 30)
        );
        botako.addHours(
                Restaurant.Day.SUNDAY,
                LocalTime.of(9, 0),
                LocalTime.of(23, 0)
        );
        Collections.addAll(botako.getGrades(), 5, 5, 4, 5);
        Collections.addAll(botako.getPhones(), "061232568", "0649193493");
        Collections.addAll(
                botako.getComments(),
                new Comment(
                        "Divan restoran!",
                        "joca",
                        LocalDateTime.of(2017, Month.DECEMBER, 21, 21, 12, 43)
                ),
                new Comment(
                        "Sjajna muzika!",
                        "ana91",
                        LocalDateTime.of(2017, Month.DECEMBER, 15, 4, 2, 5))
        );
        botako.getGrades().addAll(Arrays.asList(5, 5, 4, 3, 5, 5));

        Meal vesuvio = new Meal(
                "Vesuvio",
                Category.ITALIAN_FOOD,
                Type.PIZZA,
                "Klasična pica sa šunkom",
                800,
                botako
        );
        vesuvio.getImages().add(R.drawable.botakovesuvio);

        Collections.addAll(vesuvio.getIngredients(),
                "pelat", "sir", "praška šunka", "svež paradajz", "masline", "origano", "susam");
        vesuvio.getComments().addAll(Arrays.asList(
                new Comment(
                        "Vrlo ukusno!",
                        "boki43",
                        LocalDateTime.of(2017, Month.DECEMBER, 21, 21, 12, 43)
                ),
                new Comment(
                        "Neprofesionalno. Čekao sam više od sat vremena i na kraju je pica stigla hladna. Oprez.",
                        "MilosJovanovic",
                        LocalDateTime.of(2017, Month.JANUARY, 11, 23, 11, 23)
                ),
                new Comment(
                        "nije lose samo dve masline",
                        "JohnMcClane",
                        LocalDateTime.of(2017, Month.SEPTEMBER, 1, 15, 22, 15)),
                new Comment(
                        "ok, ne stede na salami za razliku od mnogih, narucio bih ponovo",
                        "jovan_polje",
                        LocalDateTime.of(2017, Month.JULY, 4, 18, 1, 12)
                ),
                new Comment(
                        "Testo je gletavo i nista ne valja",
                        "maja94",
                        LocalDateTime.of(2017, Month.APRIL, 1, 12, 4, 17)
                )
        ));

        Meal serbiana = new Meal(
                "Serbiana",
                Category.ITALIAN_FOOD,
                Type.PIZZA,
                "Srpska pica sa kulenom i pavlakom",
                900,
                botako
        );
        Collections.addAll(serbiana.getIngredients(),
                "pelat", "sir", "pančeta", "šampinjoni", "pavlaka", "kulen", "ljute papričice", "origano", "susam");

        Meal carbonara = new Meal(
                "Carbonara",
                Category.ITALIAN_FOOD,
                Type.PASTA,
                "Kremasta bela pasta",
                500,
                botako
        );
        Collections.addAll(
                carbonara.getIngredients(),
                "maslinovo ulje", "beli luk", "žumance", "panna", "pančeta", "šunka", "parmezan", "bosiljak", "peršun");

        Meal gambori = new Meal(
                "Gambori sos",
                Category.ITALIAN_FOOD,
                Type.PASTA,
                "Ukusni plodovi mora",
                670,
                botako
        );
        Collections.addAll(gambori.getIngredients(),
                "maslinovo ulje", "beli luk", "gambori", "paradajz sos", "peršun", "parmezan");

        Collections.addAll(botako.getMeals(), vesuvio, serbiana, carbonara, gambori);
        Integer botakoImg = R.drawable.botako,
                botakoImg2 = R.drawable.botako2,
                botakoImg3 = R.drawable.botako3;
        Collections.addAll(botako.getImages(), botakoImg, botakoImg2, botakoImg3);
        /* BOTAKO */

        /* LITTLE BAY */
        Restaurant littleBay = new Restaurant(
                "Little Bay",
                "Dositejeva 9",
                Category.GOURMAND,
                "Little Bay restoran je deo istoimenog lanca restorana u Londonu i Brajtonu. Posle četiri" +
                        " uspešna Little Bay restorana u Londonu njihov vlasnik Dragiša Piter Ilić rešio je " +
                        "da domovini prenese deo svog bogatog iskustva stečenog u decenijskom radu u oštroj" +
                        " konkurenciji svetske prestonice ugostiteljstva.",
                1500,
                60,
                44.817990,
                20.461873
        );
        littleBay.addHours(
                Restaurant.Day.WORKDAY,
                LocalTime.of(17, 0),
                LocalTime.of(23, 30)
        );
        littleBay.addHours(
                Restaurant.Day.SATURDAY,
                LocalTime.of(16, 0),
                LocalTime.of(22, 30)
        );
        littleBay.addHours(
                Restaurant.Day.SUNDAY,
                LocalTime.of(18, 0),
                LocalTime.of(23, 30)
        );
        Collections.addAll(littleBay.getGrades(), 5);
        Collections.addAll(littleBay.getPhones(), "061265568", "0642193493");

        Meal falafel = new Meal(
                "Falafel",
                Category.WORLD_FOOD,
                Type.STARTER,
                "Ukusno arapsko jelo",
                300,
                littleBay
        );
        Collections.addAll(falafel.getIngredients(),
                "leblebija", "beli luk", "tucana paprika");

        Meal penePasta = new Meal(
                "Pene pasta sa piletinom",
                Category.ITALIAN_FOOD,
                Type.PASTA,
                "Pasta sa piletinom",
                600,
                littleBay
        );
        Collections.addAll(penePasta.getIngredients(),
                "pelat", "sir", "pančeta", "šampinjoni", "pavlaka", "kulen", "ljute papričice", "origano", "susam");

        Meal duck = new Meal(
                "Pačetina u lisnatom testu",
                Category.GOURMAND,
                Type.GRILL,
                "Pečena patka",
                1500,
                littleBay
        );
        Collections.addAll(duck.getIngredients(),
                "patka", "praziluk", "crveni kupus");
        duck.getImages().add(R.drawable.littlebaypacetina);

        Meal steak = new Meal(
                "Biftek",
                Category.GOURMAND,
                Type.GRILL,
                "Sočna t-bone poslastica",
                1880,
                littleBay
        );
        Collections.addAll(steak.getIngredients(),
                "stejk", "rukola", "čeri paradajz", "parmezan", "ulje od tartufa"
        );

        Integer littleBayImg = R.drawable.littlebay,
                littleBayImg2 = R.drawable.littlebay2,
                littleBayImg3 = R.drawable.littlebay3;

        Collections.addAll(littleBay.getImages(), littleBayImg, littleBayImg2, littleBayImg3);

        Collections.addAll(littleBay.getMeals(), falafel, penePasta, duck, steak);

        /* LITTLE BAY */


        Collections.addAll(restaurants, botako, littleBay);


        /*
         * narudžbine
         * */

        Order o1 = new Order(
                mi,
                botako,
                LocalDateTime.of(2017, 11, 23, 23, 11, 43),
                "pas ne ujeda",
                LocalDateTime.of(2017, 11, 23, 23, 13, 32),
                LocalDateTime.of(2017, 11, 23, 23, 55, 1),
                Order.Status.DELIVERED
        );
        o1.getMeals().addAll(Arrays.asList(
                new OrderedMeal(serbiana, 1)
        ));
        Order o2 = new Order(
                mi,
                botako,
                LocalDateTime.of(2017, 12, 3, 11, 22, 33),
                "pas ne ujeda",
                LocalDateTime.of(2017, 12, 3, 11, 55, 1),
                LocalDateTime.of(2017, 12, 3, 12, 30, 5),
                Order.Status.DELIVERED
        );
        o2.getMeals().addAll(Arrays.asList(
                new OrderedMeal(serbiana, 2),
                new OrderedMeal(carbonara, 1)
        ));
        o2.setRestaurantToUserKm(1000);
        Order o3 = new Order(
                mi,
                littleBay,
                LocalDateTime.of(2017, 12, 9, 18, 30, 43),
                "pas ujeda",
                LocalDateTime.of(2017, 12, 9, 18, 32, 32),
                LocalDateTime.of(2017, 12, 9, 19, 15, 1)
        );
        o3.getMeals().addAll(Arrays.asList(
                new OrderedMeal(penePasta, 1),
                new OrderedMeal(steak, 1)
        ));
        o3.setRestaurantToUserKm(2000);


        Order o4 = new Order(
                mi,
                littleBay,
                LocalDateTime.of(2018, 12, 9, 18, 30, 43),
                "hajmo opet"

        );
        o4.getMeals().addAll(Arrays.asList(
                new OrderedMeal(penePasta, 3),
                new OrderedMeal(steak, 1)
        ));
        o4.setRestaurantToUserKm(3000);

        Order o5 = new Order(
                mi,
                botako,
                LocalDateTime.of(2018, 12, 9, 18, 30, 43),
                "kasnimo"
        );
        o5.getMeals().addAll(Arrays.asList(
                new OrderedMeal(penePasta, 1),
                new OrderedMeal(steak, 3)
        ));
        o5.setRestaurantToUserKm(5000);

        /*
        orders.addAll(Arrays.asList(o1, o2, o3, o4, o5));
        checkout.add(new OrderedMeal(penePasta, 5));
        checkout.add(new OrderedMeal(vesuvio, 2));
        checkout.add(new OrderedMeal(vesuvio, 2));
        checkout.add(new OrderedMeal(vesuvio, 2));
        checkout.add(new OrderedMeal(vesuvio, 2));
        checkout.add(new OrderedMeal(vesuvio, 2));
        checkout.add(new OrderedMeal(vesuvio, 2));
        */

        selectedRestaurant = botako;

        loginDeliverer();
    }

    public static int getPriciestMeal() {
        return restaurants.stream().map(Restaurant::getMinMealPrice).min(Integer::min).get();
    }

    public static int getCheapestMeal() {
        return restaurants.stream().map(Restaurant::getMaxMealPrice).max(Integer::max).get();
    }

    public static LocalTime getTime() {
        return LocalTime.now();
    }

    public static LocalDateTime getDateTime() {
        return LocalDateTime.now();
    }

    public static DayOfWeek getDayOfWeek() {
        return LocalDateTime.now().getDayOfWeek();
    }

    public static void logOff() {
        loggedOn = NOT_LOGGED_ON;
        DB.checkout.clear();
    }

    public static void loginCustomer() {
        loggedOn = mi;
        DB.checkout.clear();
    }

    private static void loginDeliverer() {
        loggedOn = ni;
        DB.checkout.clear();
    }

    public static Restaurant getRestaurantFromTitle(String title) {
        return restaurants.stream().filter(r -> r.getTitle().equals(title)).findFirst().orElse(null);
    }

    public static Order getOrderFromID(int ID) {
        for (Order o : orders)
            if (o.getID() == ID)
                return o;
        return null;
    }

    public static List<Order> getAcceptedOrders() {
        List<Order> orders = new ArrayList<>();
        for (Order o : DB.orders)
            if (o.getStatus().equals(Order.Status.ACCEPTED))
                orders.add(o);
        return orders;
    }

    public static List<Order> getMadeOrders() {
        List<Order> orders = new ArrayList<>();
        for (Order o : DB.orders)
            if (o.getStatus().equals(Order.Status.MADE))
                orders.add(o);
        return orders;
    }

    public static boolean isLoggedIn() {
        return DB.loggedOn != DB.NOT_LOGGED_ON;
    }
}
