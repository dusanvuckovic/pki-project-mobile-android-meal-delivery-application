package pki.mobile.model;

public enum Type {

    NONE("Svi tipovi"),
    PIZZA("Pica"),
    PASTA("Pasta"),
    BAGEL("Bejgl"),
    GRILL("Roštilj"),
    STARTER("Predjelo");

    private final String fullName;

    Type(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public String toString() {
        return fullName;
    }
}