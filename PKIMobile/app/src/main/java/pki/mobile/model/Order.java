package pki.mobile.model;

import com.google.android.gms.maps.model.LatLng;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

public class Order {

    private static int globalID = 0;

    private int ID;
    private List<OrderedMeal> meals;
    private User userWhoOrdered;
    private Restaurant restaurantDelivering;
    private LocalDateTime orderedOn;
    private String messageToDeliverer;
    private Status status;
    private LocalDateTime acceptedOn;
    private LocalDateTime deliveredOrDeniedOn;
    private Double restaurantToUserKm = null;
    private Double delivererToRestaurantKm = null;
    private String deliveredTo = "";
    private LatLng deliveryLatLng = null;

    public Order(User userWhoOrdered, Restaurant restaurantDelivering, LocalDateTime orderedOn, String messageToDeliverer, LocalDateTime acceptedOn, LocalDateTime deliveredOrDeniedOn) {
        this.ID = globalID++;
        this.userWhoOrdered = userWhoOrdered;
        this.meals = new LinkedList<>();
        this.restaurantDelivering = restaurantDelivering;
        this.orderedOn = orderedOn;
        this.messageToDeliverer = messageToDeliverer;
        this.acceptedOn = acceptedOn;
        this.deliveredOrDeniedOn = deliveredOrDeniedOn;
        this.status = Status.MADE;
        this.deliveredTo = userWhoOrdered.getAddress();
        this.deliveryLatLng = userWhoOrdered.getLatLng();
    }

    public Order(User userWhoOrdered, Restaurant restaurantDelivering, LocalDateTime orderedOn, String messageToDeliverer) {
        this.ID = globalID++;
        this.userWhoOrdered = userWhoOrdered;
        this.meals = new LinkedList<>();
        this.restaurantDelivering = restaurantDelivering;
        this.orderedOn = orderedOn;
        this.messageToDeliverer = messageToDeliverer;
        this.status = Status.MADE;
        this.deliveredTo = userWhoOrdered.getAddress();
        this.deliveryLatLng = userWhoOrdered.getLatLng();
    }

    public Order(User userWhoOrdered, Restaurant restaurantDelivering, LocalDateTime orderedOn, String messageToDeliverer, LocalDateTime acceptedOn, LocalDateTime deliveredOrDeniedOn, Status status) {
        this(userWhoOrdered, restaurantDelivering, orderedOn, messageToDeliverer, acceptedOn, deliveredOrDeniedOn);
        this.status = status;
    }

    public String getDeliveredTo() {
        return deliveredTo;
    }

    public void setDeliveredTo(String deliveredTo) {
        this.deliveredTo = deliveredTo;
    }

    public String getDescriptionFromStatus() {
        switch (status) {
            case MADE:
                return "Dostavlja se!";
            case ACCEPTED:
                return "Dostavlja se!";
            case DELIVERED:
                return "Preuzeta!";
            case FAILED:
                return "Obustavljena!";
        }
        return "";
    }

    public String getTimeFromStatus() {
        switch (status) {
            case MADE:
                return "Biće preuzeta!";
            case ACCEPTED:
                return "Do: " + getTimeToDeliver();
            case DELIVERED:
                return "Dostavljena: " + deliveredOrDeniedOn.format(DateTimeFormatter.ofPattern("HH:mm:ss dd.MM.yyyy."));
            case FAILED:
                return "Obustavljena: " + deliveredOrDeniedOn.format(DateTimeFormatter.ofPattern("HH:mm:ss dd.MM.yyyy."));
        }
        return "";
    }

    private String getTimeToDeliver() {
        LocalDateTime toDeliver = orderedOn.plusMinutes(45);
        return toDeliver.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
    }

    public int calculatePrice() {
        int price = 0;
        for (OrderedMeal o : meals) {
            price += o.getHowMany() * o.getMeal().getPrice();
        }
        return price;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public List<OrderedMeal> getMeals() {
        return meals;
    }

    public void setMeals(List<OrderedMeal> meals) {
        this.meals = meals;
    }

    public User getUserWhoOrdered() {
        return userWhoOrdered;
    }

    public void setUserWhoOrdered(User userWhoOrdered) {
        this.userWhoOrdered = userWhoOrdered;
    }

    public Restaurant getRestaurantDelivering() {
        return restaurantDelivering;
    }

    public void setRestaurantDelivering(Restaurant restaurantDelivering) {
        this.restaurantDelivering = restaurantDelivering;
    }

    public LocalDateTime getOrderedOn() {
        return orderedOn;
    }

    public void setOrderedOn(LocalDateTime orderedOn) {
        this.orderedOn = orderedOn;
    }

    public String getMessageToDeliverer() {
        return messageToDeliverer;
    }

    public void setMessageToDeliverer(String messageToDeliverer) {
        this.messageToDeliverer = messageToDeliverer;
    }

    public LocalDateTime getAcceptedOn() {
        return acceptedOn;
    }

    public void setAcceptedOn(LocalDateTime acceptedOn) {
        this.acceptedOn = acceptedOn;
    }

    public LocalDateTime getDeliveredOrDeniedOn() {
        return deliveredOrDeniedOn;
    }

    public void setDeliveredOrDeniedOn(LocalDateTime deliveredOrDeniedOn) {
        this.deliveredOrDeniedOn = deliveredOrDeniedOn;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public LatLng getDeliveryLatLng() {
        return deliveryLatLng;
    }

    public void setDeliveryLatLng(LatLng deliveryLatLng) {
        this.deliveryLatLng = deliveryLatLng;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        return ID == order.ID;
    }

    public void setRestaurantToUserKm(double inMeters) {
        restaurantToUserKm = inMeters / 1000;
    }

    public void setDelivererToRestaurantKm(double inMeters) {
        delivererToRestaurantKm = inMeters / 1000;
    }

    public String getRestaurantToUserDistanceKM() {
        if (restaurantToUserKm == null)
            return "n/a";
        return String.format("%.2f km", this.restaurantToUserKm);
    }

    public String getDelivererToRestaurantDistanceKM() {
        if (delivererToRestaurantKm == null)
            return "n/a";
        return String.format("%.2f km", this.delivererToRestaurantKm);
    }

    public Double getRestaurantToUserDistance() {
        return restaurantToUserKm;
    }

    public Double getDelivererToRestaurantDistance() {
        return delivererToRestaurantKm;
    }

    @Override
    public int hashCode() {
        return ID;
    }

    public enum Status {
        MADE,
        ACCEPTED,
        DELIVERED,
        FAILED
    }
}
