package pki.mobile.model;

import java.util.List;

public class OrderedMeal {
    private Meal meal;
    private int howMany;

    public OrderedMeal(Meal meal, int howMany) {
        this.meal = meal;
        this.howMany = howMany;
    }

    public static int getMealPrice(List<? extends OrderedMeal> meals) {
        int price = 0;
        for (OrderedMeal m : meals) {
            price += (m.howMany * m.meal.getPrice());
        }
        return price;
    }

    public int getOrderedMealPrice() {
        return howMany * meal.getPrice();
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public int getHowMany() {
        return howMany;
    }

    public void setHowMany(int howMany) {
        this.howMany = howMany;
    }
}