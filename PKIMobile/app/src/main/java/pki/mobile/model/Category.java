package pki.mobile.model;

public enum Category {

    NONE("Sve kategorije"),
    ITALIAN_FOOD("Italijanska hrana"),
    WORLD_FOOD("Svetska kuhinja"),
    BAKERY("Pekara"),
    KAFANA("Kafana"),
    SEA_FOOD("Plodovi mora"),
    GOURMAND("Gurmanska hrana");

    private final String fullName;

    Category(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    @Override
    public String toString() {
        return fullName;
    }
}
