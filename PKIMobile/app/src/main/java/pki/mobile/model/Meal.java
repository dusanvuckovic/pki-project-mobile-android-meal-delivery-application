package pki.mobile.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Meal {

    private String name;
    private Category category;
    private Type type;
    private List<String> ingredients;
    private String description;
    private List<Integer> images;
    private int price;
    private List<Comment> comments;
    private Restaurant containingRestaurant;

    public Meal(String name, Category category, Type type, String description, int price, Restaurant containingRestaurant) {
        this.name = name;
        this.category = category;
        this.type = type;
        this.ingredients = new LinkedList<>();
        this.description = description;
        this.images = new LinkedList<>();
        this.price = price;
        this.comments = new LinkedList<>();
        this.containingRestaurant = containingRestaurant;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getImages() {
        return images;
    }

    public void setImages(List<Integer> images) {
        this.images = images;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public Restaurant getContainingRestaurant() {
        return containingRestaurant;
    }

    public void setContainingRestaurant(Restaurant containingRestaurant) {
        this.containingRestaurant = containingRestaurant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Meal meal = (Meal) o;
        return price == meal.price &&
                Objects.equals(name, meal.name) &&
                category == meal.category &&
                type == meal.type &&
                Objects.equals(ingredients, meal.ingredients) &&
                Objects.equals(description, meal.description) &&
                Objects.equals(images, meal.images) &&
                Objects.equals(comments, meal.comments) &&
                Objects.equals(containingRestaurant, meal.containingRestaurant);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, category, type, ingredients, description, images, price, comments, containingRestaurant);
    }

    public String getFullTypeName() {
        return type.getFullName();
    }

    public String getIngredientsInList() {
        StringBuilder sb = new StringBuilder();
        sb.append(ingredients.get(0));
        for (int i = 1; i < ingredients.size(); i++)
            sb.append(String.format(", %s", ingredients.get(i)));
        return sb.toString();
    }
}
