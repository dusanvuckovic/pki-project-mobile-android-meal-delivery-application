package pki.mobile.model;

import java.time.LocalDateTime;
import java.util.Objects;

public class Comment implements Comparable {

    private String comment;
    private String username;
    private LocalDateTime madeOn;

    public Comment(String comment, String username) {
        this.comment = comment;
        this.username = username;
        madeOn = LocalDateTime.now();
    }

    public Comment(String comment, String username, LocalDateTime madeOn) {
        this.comment = comment;
        this.username = username;
        this.madeOn = madeOn;
    }

    public Comment(String comment, User user) {
        this.comment = comment;
        this.username = user.getUsername();
        madeOn = LocalDateTime.now();
    }

    public Comment(String comment, User user, LocalDateTime madeOn) {
        this.comment = comment;
        this.username = user.getUsername();
        this.madeOn = madeOn;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getMadeOn() {
        return madeOn;
    }

    public void setMadeOn(LocalDateTime madeOn) {
        this.madeOn = madeOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment1 = (Comment) o;
        return Objects.equals(comment, comment1.comment) &&
                Objects.equals(username, comment1.username);
    }

    @Override
    public int hashCode() {
        int result = comment != null ? comment.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (madeOn != null ? madeOn.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Object o) {
        Comment c1 = this, c2 = (Comment) o;
        return c2.getMadeOn().compareTo(c1.getMadeOn());
    }
}
