import os


lines = 0
for root, dirs, files in os.walk('res\layout'):
	for f in files:
		filepath = os.path.join(root, f)
		if filepath.endswith('.xml'):
			print(filepath)
			file = open(filepath, encoding='utf-8')
			lines += len(file.readlines())
			file.close()
print("lines = {}".format(lines))
